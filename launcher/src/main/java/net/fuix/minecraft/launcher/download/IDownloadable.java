package net.fuix.minecraft.launcher.download;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Создано Sceri 04.07.2015.
 */
public interface IDownloadable {

    public File getLocalFile();

    public URL getRemoteFile();

    public void setChecksum(String checksum);

    public void setUpdated();

    public void beforeDownload(Object... data) throws IOException;

    public void afterDownload(Object... data) throws IOException;

}
