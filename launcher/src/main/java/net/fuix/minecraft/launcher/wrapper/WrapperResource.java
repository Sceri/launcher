package net.fuix.minecraft.launcher.wrapper;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.*;
import com.mojang.authlib.utils.HTTPUtils;
import net.fuix.minecraft.launcher.download.IDownloadable;
import net.fuix.minecraft.launcher.util.DirectoryUtils;
import net.fuix.minecraft.launcher.util.OperatingSystem;
import net.fuix.minecraft.launcher.wrapper.CompatibilityRule.Action;
import net.minecraft.launchwrapper.IWrapperResource;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static net.fuix.minecraft.launcher.wrapper.RepositoryType.SELECTEL;
import static net.fuix.minecraft.launcher.wrapper.RepositoryType.FUIX;
import static net.fuix.minecraft.launcher.wrapper.RepositoryType.MOJANG;

/**
 * Создано Sceri 07.01.2015.
 */
public class WrapperResource implements IWrapperResource, IDownloadable {
    private final ResourceType type;
    private final RepositoryType repositoryType;

    private final String name;

    private final String groupId;
    private final String artifactId;
    private final String version;
    private final String extension;

    private final String destPath;

    private final List<CompatibilityRule> rules;

    private File destFile;
    private File localFile;
    private URL remoteFile;

    private String checksum;
    private boolean updated;
    private CheckType checkType;

    public WrapperResource(ResourceType type, RepositoryType repositoryType, String name, String destPath, List<CompatibilityRule> rules, CheckType checkType) {
        this.type = type == null ? ResourceType.RESOURCES : type;
        this.repositoryType = repositoryType == null ? SELECTEL : repositoryType;
        this.name = name;
        this.destPath = destPath == null ? type == ResourceType.NATIVES ? "%CLIENT_DIR%/natives/" : null : destPath;
        this.checkType = checkType;
        this.rules = rules;

        checkArgument(!Strings.isNullOrEmpty(name), "Cannot get artifact filename of empty/blank artifact");

        String[] parts = StringUtils.split(name, ":", 4);

        this.groupId = parts[0];
        this.artifactId = parts[1];
        this.version = parts[2];
        this.extension = parts.length == 4 ? parts[3] : "jar";

        String classifier = type == ResourceType.NATIVES ? "natives-" + OperatingSystem.getCurrentPlatform().getName() : null;
        this.localFile = new File(DirectoryUtils.MAIN_DIRECTORY, this.type.getFolder() + File.separator + getArtifactPath(classifier));
        this.remoteFile =
                this.repositoryType == MOJANG ? HTTPUtils.constantURL(MOJANG.getUrl() + getArtifactPath(classifier)) :
                this.repositoryType == SELECTEL ? HTTPUtils.constantURL(SELECTEL.getUrl() + this.type.getFolder() + "/" + getArtifactPath(classifier)) :
                        HTTPUtils.constantURL(FUIX.getUrl() + this.type.getFolder() + "/" + getArtifactPath(classifier));
    }

    public void beforeDownload(Object... data) throws IOException {
    }

    public void afterDownload(Object... data) throws IOException {
        WrapperProfile profile = (WrapperProfile) data[0];
        destFile = destPath != null ? new File(DirectoryUtils.replaceTokens(destPath, profile)) : type == ResourceType.RESOURCES ? new File(localFile.getParentFile(), getArtifactId()) : null;
        if (destFile != null) {
            if ((!destFile.exists() || !destFile.isDirectory() || (destFile.list().length == 0)) || updated || (checkType != CheckType.DISABLED)) {
                DirectoryUtils.unzip(localFile, destFile, updated ? CheckType.DISABLED : checkType);
            }
        }
    }

    public boolean canApply() {
        for (CompatibilityRule rule : rules) {
            Action action = rule.getAppliedAction();
            if (action != null && action == Action.DISALLOW) {
                return false;
            }
        }

        return true;
    }

    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    protected String getArtifactPath() {
        return getArtifactPath(null);
    }

    protected String getArtifactPath(String classifier) {
        return String.format("%s/%s", getArtifactBaseDir(), getArtifactFilename(classifier));
    }

    protected String getArtifactBaseDir() {
        return String.format("%s/%s/%s", getGroupId().replaceAll("\\.", "/"), getArtifactId(), getVersion());
    }

    protected String getArtifactFilename(String classifier) {
        return classifier == null ? String.format("%s-%s.%s", getArtifactId(), getVersion(), getExtension()) : String.format("%s-%s%s.%s", getArtifactId(), getVersion(), "-" + classifier, getExtension());
    }

    public String getGroupId() {
        return groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getVersion() {
        return version;
    }

    public String getExtension() {
        return extension;
    }

    public String getName() {
        return name;
    }

    public String getDestPath() {
        return destPath;
    }

    public boolean isUpdated() {
        return updated;
    }

    public File getDestFile() {
        return destFile;
    }

    public File getLocalFile() {
        return localFile;
    }

    public URL getRemoteFile() {
        return remoteFile;
    }

    public ResourceType getType() {
        return type;
    }

    public CheckType getCheckType() {
        return checkType;
    }

    public List<CompatibilityRule> getRules() {
        return rules;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    @Override
    public File getFile() {
        return this.destFile != null ? this.destFile : localFile;
    }

    public void setUpdated() {
        this.updated = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WrapperResource that = (WrapperResource) o;

        return Objects.equal(this.type, that.type) &&
                Objects.equal(this.name, that.name) &&
                Objects.equal(this.destPath, that.destPath) &&
                Objects.equal(this.rules, that.rules);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(type, name, destPath, rules);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("type", type)
                .add("name", name)
                .add("destPath", destPath)
                .add("rules", rules)
                .toString();
    }

    public static class WrapperResourceAdapter implements JsonDeserializer<WrapperResource>, JsonSerializer<WrapperResource> {

        @Override
        public WrapperResource deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
            JsonObject object = jsonElement.getAsJsonObject();

            ResourceType resourceType = object.has("key") ? context.<ResourceType>deserialize(object.get("key"), ResourceType.class) : null;
            RepositoryType repositoryType = object.has("repositoryType") ? context.<RepositoryType>deserialize(object.get("repositoryType"), RepositoryType.class) : null;
            String name = object.has("name") ? object.getAsJsonPrimitive("name").getAsString() : null;
            String destPath = object.has("destPath") ? object.getAsJsonPrimitive("destPath").getAsString() : null;

            List<CompatibilityRule> rules = Lists.newArrayList();

            JsonArray array = object.has("rules") ? object.getAsJsonArray("rules") : null;
            if (array != null) {
                for (JsonElement element : array) {
                    rules.add(context.<CompatibilityRule>deserialize(element, CompatibilityRule.class));
                }
            }

            CheckType checkType = object.has("checkType") ? context.<CheckType>deserialize(object.get("checkType"), CheckType.class) : CheckType.DISABLED;

            return new WrapperResource(resourceType, repositoryType, name, destPath, rules, checkType);
        }

        @Override
        public JsonElement serialize(WrapperResource wrapperResource, Type type, JsonSerializationContext context) {
            JsonObject object = new JsonObject();

            if (wrapperResource.getRepositoryType() != SELECTEL) {
                object.add("repositoryType", context.serialize(wrapperResource.getRepositoryType(), RepositoryType.class));
            }

            object.addProperty("name", wrapperResource.getName());

            if (wrapperResource.getDestPath() != null) {
                object.addProperty("destPath", wrapperResource.getDestPath());
            }

            if (!wrapperResource.getRules().isEmpty()) {
                JsonArray array = new JsonArray();
                for (CompatibilityRule compatibilityRule : wrapperResource.getRules()) {
                    array.add(context.serialize(compatibilityRule, CompatibilityRule.class));
                }

                object.add("rules", array);
            }

            if (wrapperResource.getCheckType() != CheckType.DISABLED) {
                object.add("checkType", context.serialize(wrapperResource.getCheckType(), CheckType.class));
            }

            return object;
        }
    }
}
