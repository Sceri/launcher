package net.fuix.minecraft.launcher.util;

import com.google.common.base.Strings;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Создано Sceri 30.12.2014.
 */
public class EncryptionUtils {

    public static String getDigest(File file, String algorithm, int hashLength) {
        checkNotNull(file, "Файл {%s} должен существовать", file.getName());
        checkArgument(!Strings.isNullOrEmpty(algorithm), "Алгоритм должен быть указан!");

        MessageDigest messageDigest = DigestUtils.getDigest(algorithm);

        try (FileInputStream inputStream = FileUtils.openInputStream(file)) {
            DigestUtils.updateDigest(messageDigest, inputStream);
        } catch (IOException ex) {
            return null;
        }

        return String.format("%1$0" + hashLength + "x", new BigInteger(1, messageDigest.digest()));
    }

    public static String copyAndDigest(InputStream inputStream, OutputStream outputStream, String algorithm, int hashLength) throws IOException {
        MessageDigest messageDigest = DigestUtils.getDigest(algorithm);

        byte[] buffer = new byte[65536];
        try {
            int read = inputStream.read(buffer);
            while (read >= 1) {
                messageDigest.update(buffer, 0, read);
                outputStream.write(buffer, 0, read);
                read = inputStream.read(buffer);
            }
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }

        return String.format("%1$0" + hashLength + "x", new BigInteger(1, messageDigest.digest()));
    }

    public static String decrypt(String line, String key) {
        checkNotNull(line);
        checkNotNull(key);

        try {
            DESKeySpec keySpec = new DESKeySpec(key.getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(keySpec);

            Cipher desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            desCipher.init(Cipher.DECRYPT_MODE, secretKey);

            return new String(desCipher.doFinal(Hex.decodeHex(line.toCharArray())), "UTF-8");
        } catch (Exception ignored) {
        }

        return null;
    }

    public static String encrypt(String line, String key) {
        checkNotNull(line);
        checkNotNull(key);

        try {
            DESKeySpec keySpec = new DESKeySpec(key.getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(keySpec);

            Cipher desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            desCipher.init(Cipher.ENCRYPT_MODE, secretKey);

            return Hex.encodeHexString(desCipher.doFinal(line.getBytes()));
        } catch (Exception ignored) {
        }

        return null;
    }
}
