package net.fuix.minecraft.launcher.gui;

import com.google.common.collect.Lists;
import com.mojang.authlib.exceptions.RequestException;
import net.fuix.minecraft.launcher.download.DownloadException;
import net.fuix.minecraft.launcher.download.DownloadTask;
import net.fuix.minecraft.launcher.download.Downloader;
import net.fuix.minecraft.launcher.download.IDownloadListener;
import net.fuix.minecraft.launcher.util.ImageUtils;
import net.fuix.minecraft.launcher.util.LauncherLog;
import net.fuix.minecraft.launcher.util.LauncherUtils;
import net.fuix.minecraft.launcher.wrapper.WrapperProfile;
import org.apache.logging.log4j.Level;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Sceri 30.06.2016.
 */
public class DownloadPane extends TextPane implements IDownloadListener {
    private static final Font FONT = new Font("Arial", Font.BOLD, 18);
    private static final BufferedImage BAR_EMPTY = ImageUtils.getImage("bar_1");
    private static final BufferedImage BAR_FULL = ImageUtils.getImage("bar_2");

    private final MainPane mainPane;

    private String infoText;
    private int percent;

    public DownloadPane(final MainPane mainPane, int x, int y, int w, int h) {
        super(null, FONT, x, y, w, h);
        this.mainPane = mainPane;
    }

    @Override
    public void stop() {
        super.stop();
        removeAll();
    }

    public void setInfoText(String text) {
        this.infoText = text;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public void download(WrapperProfile profile) throws DownloadException {
        try {
            profile.startVerifying(Lists.<IDownloadListener>newArrayList(this));
        } catch (RequestException | InterruptedException e) {
            throw new DownloadException(e);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        if(animation) {
            Graphics2D graphics2D = (Graphics2D) g;
            graphics2D.setFont(FONT);

            String clientUpdate = "Обновление клиента";
            int x = getWidth() / 2 - (graphics2D.getFontMetrics().stringWidth(clientUpdate) / 2);
            int y = 40;

            ImageUtils.paintString(clientUpdate, x, y, g, new Color(255, 212, 0), new Color(50, 50, 50));

            graphics2D.setFont(new Font("Arial", Font.BOLD, 14));
            graphics2D.setColor(Color.WHITE);

            if (infoText != null) {
                ImageUtils.paintString(infoText, getWidth() / 2 - (graphics2D.getFontMetrics().stringWidth(infoText) / 2), 60, g, new Color(255, 212, 0), new Color(50, 50, 50));
            }

            graphics2D.drawImage(BAR_EMPTY, getWidth() / 2 - (BAR_EMPTY.getWidth() / 2), 80, null);

            if (percent != 0) {
                BufferedImage image = BAR_FULL.getSubimage(0, 0, (int) (((double) BAR_FULL.getWidth() / 100.0D) * (double) percent), BAR_FULL.getHeight());

                graphics2D.drawImage(image, getWidth() / 2 - (BAR_FULL.getWidth() / 2), 80, null);
            }
        }

        super.paintComponent(g);
    }

    @Override
    public void onDownloadFinished(Downloader downloader) throws DownloadException {
        int failures = downloader.getFailures().size();
        if (failures > 0) {
            for (DownloadTask task : downloader.getFailures()) {
                LauncherLog.log(Level.ERROR, task.getLastException(), "Не удалось загрузить файл %s", task.getDownloadable().getRemoteFile().toString());
            }
            LauncherUtils.showError(String.format("Не удалось загрузить %s %s.", failures, LauncherUtils.plural(failures, new String[]{"файл", "файла", "файлов"})));

            throw new DownloadException("Не удалось обновить клиент");
        }
    }

    private synchronized void updateProgress(Downloader downloader) {
        long current = 0L;
        long total = 0L;

        for (DownloadTask task : downloader.getAllFiles()) {
            current += task.getCurrent();
            total += task.getTotal();
        }

        int percent = (int) ((float) current / (float) total * 100.0F);

        setInfoText("Загрузка файлов (" + downloader.getSuccessful().size() + " из " + downloader.getAllFiles().size() + ") - " + percent + "%");
        setPercent(percent);
    }

    @Override
    public void onDownloadProgressChanged(Downloader downloader) {
        updateProgress(downloader);
    }
}
