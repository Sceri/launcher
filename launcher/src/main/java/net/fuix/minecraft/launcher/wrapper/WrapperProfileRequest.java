package net.fuix.minecraft.launcher.wrapper;

import com.google.gson.annotations.SerializedName;

/**
 * Создано Sceri 10.02.2015.
 */
public class WrapperProfileRequest {

    @SerializedName("profileName")
    private final String profileName;

    public WrapperProfileRequest(String profileName) {
        this.profileName = profileName;
    }

    public String getProfileName() {
        return profileName;
    }
}
