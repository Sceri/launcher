package net.fuix.minecraft.launcher.wrapper;

/**
 * Создано Sceri 07.07.2015.
 */
public enum RepositoryType {
    MOJANG("https://libraries.minecraft.net/"), SELECTEL("http://142408.selcdn.com/centurymine/"), FUIX("http://files.fuix.net/");

    final String url;

    RepositoryType(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

}