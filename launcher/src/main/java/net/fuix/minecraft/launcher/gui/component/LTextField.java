package net.fuix.minecraft.launcher.gui.component;

import net.fuix.minecraft.launcher.gui.MainPane;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.image.BufferedImage;

public class LTextField extends JTextField {

    public LTextField() {
        setOpaque(false);
        setFont(MainPane.FONT);
        setBorder(new EmptyBorder(5, 10, 5, 10));
        setForeground(MainPane.ORANGE);
        setCaretColor(MainPane.ORANGE);
    }

}
