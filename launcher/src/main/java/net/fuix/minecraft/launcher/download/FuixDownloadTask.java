package net.fuix.minecraft.launcher.download;

import com.mojang.authlib.utils.HTTPUtils;
import net.fuix.minecraft.launcher.util.DataClass;
import net.fuix.minecraft.launcher.util.EncryptionUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * Created by Sceri 06.07.2016.
 */
public class FuixDownloadTask extends DownloadTask {
    public FuixDownloadTask(IDownloadable downloadable, DataClass dataClass) {
        super(downloadable, dataClass);
    }

    @Override
    public void download() throws DownloadException {
        incrementAttempts();
        ensureFileWritable(downloadable.getLocalFile());

        try {
            downloadable.beforeDownload(dataClass.objects);

            String checksum = HTTPUtils.performGetRequest(HTTPUtils.constantURL(downloadable.getRemoteFile().toString() + ".md5"));
            if(checksum.equals(EncryptionUtils.getDigest(downloadable.getLocalFile(), "MD5", 32))) {
                downloadable.afterDownload(dataClass.objects);
                return;
            }

            HttpURLConnection connection = HTTPUtils.createUrlConnection(downloadable.getRemoteFile());
            int status = connection.getResponseCode();

            if (status / 100 == 2) {
                updateExpectedSize(connection);
                InputStream inputStream = new MonitoringInputStream(connection.getInputStream(), this);
                FileOutputStream outputStream = new FileOutputStream(downloadable.getLocalFile());
                String md5 = EncryptionUtils.copyAndDigest(inputStream, outputStream, "MD5", 32);

                if (checksum.equalsIgnoreCase(md5)) {
                    downloadable.setChecksum(md5);
                    downloadable.setUpdated();
                    downloadable.afterDownload(dataClass.objects);
                    return;
                }

                throw new DownloadException(String.format("Хешсумма скаченного файла (%s) не совпадает с удалённой (%s)", md5, checksum));
            }

            throw new DownloadException(String.format("Не удалось загрузить файл (%s). Ответ сервера: %s", downloadable.getRemoteFile(), status));
        } catch (IOException e) {
            throw new DownloadException(String.format("Не удалось загрузить файл (%s)", downloadable.getRemoteFile()), e);
        }
    }
}
