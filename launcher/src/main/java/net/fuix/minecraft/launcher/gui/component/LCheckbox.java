package net.fuix.minecraft.launcher.gui.component;

import net.fuix.minecraft.launcher.gui.MainPane;

import javax.swing.*;
import java.awt.*;

public class LCheckbox extends JCheckBox {

    public LCheckbox(String title) {
        super(title);

        setBorder(null);
        setOpaque(false);
        setFocusable(false);
        setFont(MainPane.FONT);
        setForeground(Color.WHITE);
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
}
