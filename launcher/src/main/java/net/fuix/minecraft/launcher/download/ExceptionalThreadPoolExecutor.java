package net.fuix.minecraft.launcher.download;

import java.util.concurrent.*;

/**
 * Создано Sceri 09.01.2015.
 */
public class ExceptionalThreadPoolExecutor extends ThreadPoolExecutor {

    public ExceptionalThreadPoolExecutor(int threadCount) {
        super(threadCount, threadCount, 30L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);

        if ((t == null) && ((r instanceof Future)))
            try {
                Future future = (Future) r;
                if (future.isDone())
                    future.get();
            } catch (CancellationException | ExecutionException ignored) {
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }
    }

    @Override
    protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
        return new ExceptionalFutureTask(runnable, value);
    }

    @Override
    protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new ExceptionalFutureTask(callable);
    }

    public class ExceptionalFutureTask<T> extends FutureTask<T> {
        public ExceptionalFutureTask(Callable<T> callable) {
            super(callable);
        }

        public ExceptionalFutureTask(Runnable runnable, T result) {
            super(runnable, result);
        }

        @Override
        protected void done() {
            try {
                get();
            } catch (InterruptedException | ExecutionException t) {
                System.out.println("Unhandled exception in executor: " + t.getMessage());
            }
        }
    }
}
