package net.fuix.minecraft.launcher.gui;


import net.fuix.minecraft.launcher.util.ImageUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Sceri 30.06.2016.
 */
public class TextPane extends JPanel {
    private String text;
    private Font font;
    private int ticker = 0;
    protected boolean animation = true;

    public TextPane(String text, Font font, int x, int y, int w, int h) {
        super(null);

        this.text = text;
        this.font = font;

        setOpaque(false);
        setBounds(x, y, w, h);
    }

    public void setText(String text) {
        this.text = text;
    }

    public void stop() {
        animation = false;
    }

    public void incTicker() {
        if(ticker == 2) {
            ticker = -1;
        }

        ticker++;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;

        if(text != null) {
            String dots = "";
            if (animation) {
                switch (ticker) {
                    case 0:
                        dots = ".";
                        break;
                    case 1:
                        dots = "..";
                        break;
                    case 2:
                        dots = "...";
                        break;
                }
            }

            String string = text + dots;
            graphics2D.setFont(font);

            int x = getWidth() / 2 - (graphics2D.getFontMetrics().stringWidth(string) / 2);
            int y = getHeight() / 2 - (graphics2D.getFontMetrics().getHeight() / 2);

            ImageUtils.paintString(string, x, y, g, new Color(255, 212, 0), new Color(50, 50, 50));
        }
    }

}
