package net.fuix.minecraft.launcher.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Sceri 30.06.2016.
 */
public class ImageUtils {

    public static BufferedImage getImage(String name) {
        try {
            return ImageIO.read(ImageUtils.class.getResource("/resources/" + name + ".png"));
        } catch (Exception e) {
            LauncherUtils.showAndLogError(String.format("Не удалось загрузить картинку %s", name), e, true);
        }

        return new BufferedImage(0, 0, 0);
    }

    public static void paintString(String text, int x, int y, Graphics g, Color textColor, Color shadow) {
        g.setColor(shadow);
        g.drawString(text, shiftEast(x, 2), shiftSouth(y, 2));
        g.setColor(textColor);
        g.drawString(text, x, y);
    }

    private static int shiftNorth(int p, int distance) {
        return (p - distance);
    }

    private static int shiftSouth(int p, int distance) {
        return (p + distance);
    }

    private static int shiftEast(int p, int distance) {
        return (p + distance);
    }

    private static int shiftWest(int p, int distance) {
        return (p - distance);
    }
}
