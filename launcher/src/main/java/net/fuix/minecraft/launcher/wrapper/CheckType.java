package net.fuix.minecraft.launcher.wrapper;

/**
 * Создано Sceri 26.11.2015.
 */
public enum CheckType {
    DISABLED, LIGHT, HARD
}
