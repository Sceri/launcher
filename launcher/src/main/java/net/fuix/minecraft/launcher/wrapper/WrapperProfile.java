package net.fuix.minecraft.launcher.wrapper;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.*;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.mojang.authlib.Response;
import com.mojang.authlib.exceptions.RequestException;
import com.mojang.authlib.utils.HTTPUtils;
import net.fuix.minecraft.launcher.Launcher;
import net.fuix.minecraft.launcher.download.*;
import net.fuix.minecraft.launcher.util.DataClass;
import net.fuix.minecraft.launcher.util.DirectoryUtils;
import net.minecraft.launchwrapper.*;
import net.minecraft.launchwrapper.exceptions.WrapperException;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.List;


/**
 * Создано Sceri 04.01.2015.
 */
public class WrapperProfile extends Response implements IWrapperProfile {
    protected String name;

    protected Multimap<ResourceType, IWrapperResource> wrapperResources;
    protected IWrapperResource assets;

    protected List<String> tweakers;
    protected List<ServerInfo> servers;
    protected List<String> launchArgs = Lists.newArrayList();

    protected String mainClass;
    protected String version;

    protected File clientDirectory;
    protected File configsDirectory;
    protected File nativesDirectory;

    protected boolean isCompleted = false;

    public WrapperProfile() {
        super();
    }

    public WrapperProfile(String name) {
        this.name = name;
    }

    public WrapperProfile(String name, String mainClass, String version, Multimap<ResourceType, IWrapperResource> wrapperResources, IWrapperResource assets, List<String> tweakers,  List<ServerInfo> servers) {
        this.name = name;
        this.mainClass = mainClass;
        this.version = version;
        this.tweakers = tweakers;
        this.assets = assets;

        this.clientDirectory = DirectoryUtils.getClientDirectory(this);
        this.configsDirectory = new File(clientDirectory, "configs");
        this.nativesDirectory = new File(clientDirectory, "natives");

        this.wrapperResources = Multimaps.filterValues(wrapperResources, new Predicate<IWrapperResource>() {
            @Override
            public boolean apply(IWrapperResource input) {
                WrapperResource resource = (WrapperResource) input;
                return resource.canApply();
            }
        });

        this.servers = servers;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    @Override
    public File getDirectory() {
        return clientDirectory;
    }

    @Override
    public File getAssetsDirectory() {
        return assets != null ? assets.getFile() : new File(clientDirectory, "assets");
    }

    @Override
    public File getConfigsDirectory() {
        return configsDirectory;
    }

    @Override
    public File getNativesDirectory() {
        return nativesDirectory;
    }

    @Override
    public URL[] getClassPath() {
        //Бог сортировки
        List<URL> urls = Ordering.natural().onResultOf(new Function<URL, Integer>() {
            @Override
            public Integer apply(URL from) {
                if (FilenameUtils.getBaseName(from.getFile()).startsWith("beacon") || FilenameUtils.getBaseName(from.getFile()).startsWith(getName() + "-Assets")) {
                    return 0;
                }

                return 1;
            }
        }).sortedCopy(Iterables.filter(Iterables.transform(getLibraries(), new Function<IWrapperResource, URL>() {
            @Override
            public URL apply(IWrapperResource resource) {
                try {
                    return resource.getFile().toURI().toURL();
                } catch (MalformedURLException ignored) {
                }

                return null;
            }

        }), Predicates.notNull()));

        return Iterables.toArray(urls, URL.class);
    }

    public void makeCompleted() throws RequestException {
        WrapperProfile wrapperProfile = HTTPUtils.makeRequest(Launcher.WRAPPER_GSON, Launcher.WRAPPER_PROFILE_URL, new WrapperProfileRequest(getName()), WrapperProfile.class);

        this.wrapperResources = wrapperProfile.wrapperResources;
        this.assets = wrapperProfile.assets;
        this.tweakers = wrapperProfile.tweakers;
        this.servers = wrapperProfile.servers;
        this.mainClass = wrapperProfile.mainClass;
        this.version = wrapperProfile.version;
        this.clientDirectory = wrapperProfile.clientDirectory;
        this.configsDirectory = wrapperProfile.configsDirectory;
        this.nativesDirectory = wrapperProfile.nativesDirectory;
        this.isCompleted = true;
    }

    public void startVerifying(List<IDownloadListener> listeners) throws DownloadException, RequestException, InterruptedException {
        if (!isCompleted()) {
            makeCompleted();
        }

        Downloader downloader = new Downloader(listeners);

        final DataClass dataClass = new DataClass(this);

        wrapperResources.put(ResourceType.RESOURCES, assets);
        downloader.addDownloadTasks(Lists.newArrayList(Iterables.transform(wrapperResources.values(), new Function<IWrapperResource, DownloadTask>() {
            @Override
            public DownloadTask apply(IWrapperResource input) {
                WrapperResource resource = (WrapperResource) input;
                return resource.getRepositoryType() == RepositoryType.FUIX ? new FuixDownloadTask(resource, dataClass) : new CDNDownloadTask(resource, dataClass);
            }
        })));

        downloader.startDownloading(new ExceptionalThreadPoolExecutor(16));
    }

    public void launch(ILauncher launcher, boolean isClient) throws WrapperException {
        WrapperManager manager = isClient ? WrapperManager.getInstance().startClient(launcher, this) : WrapperManager.getInstance().startServer(launcher, this);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getMainClass() {
        return mainClass;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public List<String> getTweakers() {
        return tweakers;
    }

    @Override
    public List<String> getLaunchArgs() {
        return launchArgs;
    }

    @Override
    public List<ServerInfo> getServers() {
        return servers;
    }

//    public void setLaunchArgs(List<String> launchArgs) {
//        this.launchArgs = launchArgs;
//    }

    public Multimap<ResourceType, IWrapperResource> getWrapperResources() {
        return wrapperResources;
    }

    @Override
    public Collection<IWrapperResource> getLibraries() {
        return wrapperResources.get(ResourceType.LIBRARIES);
    }

    @Override
    public Collection<IWrapperResource> getMods() {
        return wrapperResources.get(ResourceType.MODS);
    }

    @Override
    public Collection<IWrapperResource> getPlugins() {
        return wrapperResources.get(ResourceType.PLUGINS);
    }

    @Override
    public Collection<IWrapperResource> getNatives() {
        return wrapperResources.get(ResourceType.NATIVES);
    }

    @Override
    public Collection<IWrapperResource> getResources() {
        return wrapperResources.get(ResourceType.RESOURCES);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WrapperProfile that = (WrapperProfile) o;

        return Objects.equal(this.name, that.name) &&
                Objects.equal(this.mainClass, that.mainClass) &&
                Objects.equal(this.version, that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, mainClass, version);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("name", name)
                .add("mainClass", mainClass)
                .add("version", version)
                .add("wrapperResources", wrapperResources)
                .add("isCompleted", isCompleted)
                .toString();
    }

    public static class WrapperProfileAdapter implements JsonDeserializer<WrapperProfile>, JsonSerializer<WrapperProfile> {
        private final Type multimapType = new TypeToken<Multimap<ResourceType, IWrapperResource>>() {
        }.getType();
        private final Type tweakersType = new TypeToken<List<String>>() {
        }.getType();
        private final Type serversType = new TypeToken<List<ServerInfo>>() {
        }.getType();

        @Override
        public WrapperProfile deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
            JsonObject object = jsonElement.getAsJsonObject();

            String name = object.has("name") ? object.get("name").getAsString() : null;
            String version = object.has("version") ? object.get("version").getAsString() : null;
            String mainClass = object.has("mainClass") ? object.get("mainClass").getAsString() : null;

            Multimap<ResourceType, IWrapperResource> wrapperResources = object.has("wrapperResources") ? context.<Multimap<ResourceType, IWrapperResource>>deserialize(object.get("wrapperResources"), multimapType) : null;
            IWrapperResource assets = object.has("assets") ? context.<IWrapperResource>deserialize(object.get("assets"), IWrapperResource.class) : null;

            List<String> tweakers = object.has("tweakers") ? context.<List<String>>deserialize(object.get("tweakers"), tweakersType) : null;
            List<ServerInfo> servers = object.has("servers") ? context.<List<ServerInfo>>deserialize(object.get("servers"), serversType) : Lists.<ServerInfo>newArrayList();

            if (name == null) {
                WrapperProfile profile = new WrapperProfile();
                profile.setError(object.get("error").getAsString());
                profile.setErrorMessage(object.get("errorMessage").getAsString());

                return profile;
            }

            if (version == null || mainClass == null || wrapperResources == null || assets == null || tweakers == null) {
                return new WrapperProfile(name);
            }

            return new WrapperProfile(name, mainClass, version, wrapperResources, assets, tweakers, servers);
        }

        @Override
        public JsonElement serialize(WrapperProfile wrapperProfile, Type type, JsonSerializationContext context) {
            JsonObject object = new JsonObject();

            object.addProperty("name", wrapperProfile.name);
            object.addProperty("version", wrapperProfile.version);
            object.addProperty("mainClass", wrapperProfile.mainClass);
            object.add("wrapperResources", context.serialize(wrapperProfile.wrapperResources, multimapType));
            if (wrapperProfile.assets != null)
                object.add("assets", context.serialize(wrapperProfile.assets, IWrapperResource.class));
            object.add("tweakers", context.serialize(wrapperProfile.tweakers, tweakersType));
            object.add("tweakers", context.serialize(wrapperProfile.servers, serversType));

            return object;
        }
    }
}

