package net.fuix.minecraft.launcher.download;

import com.mojang.authlib.utils.HTTPUtils;
import net.fuix.minecraft.launcher.util.DataClass;
import net.fuix.minecraft.launcher.util.EncryptionUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * Создано Sceri 09.01.2015.
 */
public abstract class DownloadTask {
    protected final IDownloadable downloadable;
    protected final DataClass dataClass;

    protected Downloader downloader;

    protected int attempts = 1;

    protected long startTime;
    protected long endTime;
    protected long total;
    protected long current;
    protected long expectedSize;

    protected Exception lastException;

    public DownloadTask(IDownloadable downloadable, DataClass dataClass) {
        this.downloadable = downloadable;
        this.dataClass = dataClass;
    }

    public static String getETag(String eTag) {
        if (eTag == null) {
            eTag = "-";
        } else if ((eTag.startsWith("\"")) && (eTag.endsWith("\""))) {
            eTag = eTag.substring(1, eTag.length() - 1);
        }
        return eTag;
    }

    public abstract void download() throws DownloadException;

    protected void ensureFileWritable(File target) throws DownloadException {
        if (target.getParentFile() != null && !target.getParentFile().exists()) {
            try {
                FileUtils.forceMkdir(target.getParentFile());
            } catch (IOException e) {
                throw new DownloadException(String.format("Не удалось создать директорию %s", target.getParentFile().getAbsolutePath()), e);
            }
        }

        if (target.isFile() && !target.canWrite()) {
            throw new DownloadException(String.format("Нет разрешения на запись файла %s", target.getAbsolutePath()));
        }
    }

    protected void updateExpectedSize(HttpURLConnection connection) {
        setTotal(connection.getContentLength());
        setExpectedSize(connection.getContentLength());
    }

    public Downloader getDownloader() {
        return downloader;
    }

    public void setDownloader(Downloader downloader) {
        this.downloader = downloader;
    }

    public long getExpectedSize() {
        return expectedSize;
    }

    public void setExpectedSize(long expectedSize) {
        this.expectedSize = expectedSize;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
        if (this.current > this.total) {
            this.total = current;
        }

        this.downloader.updateProgress();
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
        this.downloader.updateProgress();
    }

    public void addProgress(long amount) {
        setCurrent(getCurrent() + amount);
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public Exception getLastException() {
        return lastException;
    }

    public void setLastException(Exception lastException) {
        this.lastException = lastException;
    }

    public IDownloadable getDownloadable() {
        return downloadable;
    }

    public void incrementAttempts() {
        setAttempts(getAttempts() + 1);
    }
}
