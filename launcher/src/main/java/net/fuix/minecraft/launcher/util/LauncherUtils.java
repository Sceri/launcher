package net.fuix.minecraft.launcher.util;

import com.github.sarxos.winreg.HKey;
import com.github.sarxos.winreg.RegistryException;
import com.github.sarxos.winreg.WindowsRegistry;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.mojang.authlib.utils.HTTPUtils;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Level;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.UUID;

/**
 * Создано Sceri 09.07.2015.
 */
public class LauncherUtils {
    private static final Gson GSON = new GsonBuilder().create();
    private static final File CONFIG_FILE = new File(DirectoryUtils.MAIN_DIRECTORY, "launcher.json");
    private static final Config CONFIG = loadConfiguration();

    public static void showAndLogError(String message, Exception exception) {
        LauncherLog.log(Level.ERROR, exception, message);
        showError(message);
    }

    public static void showAndLogError(String message, Exception exception, boolean exit) {
        LauncherLog.log(Level.ERROR, exception, message);
        showError(message, exit);
    }

    public static void showError(String message) {
        showError(message, false);
    }

    public static String plural(int n, String[] titles) {
        return titles[(n = (n = n % 100) > 19 ? (n % 10) : n) == 1 ? 0 : ((n > 1 && n <= 4) ? 1 : 2)];
    }

    public static void showError(String message, boolean exit) {
        final File file = new File(DirectoryUtils.MAIN_DIRECTORY, "logs");
        try {
            JLabel label = new JLabel();
            StringBuilder style = new StringBuilder("font-family:" + label.getFont().getFamily() + ";");
            style.append("font-weight:").append(label.getFont().isBold() ? "bold" : "normal").append(";");
            style.append("font-size:").append(label.getFont().getSize()).append("pt;");

            JEditorPane ep = new JEditorPane("text/html", String.format("<html><body style=\"%s\"><center>Ошибка: %s<br>Создайте тему на форуме прикрепив к ней последний лог из <a href=\"%s\">папки</a> если подобное повторится</center></body></html>", style, message, file.toURI().toURL()));
            ep.addHyperlinkListener(new HyperlinkListener()
            {
                @Override
                public void hyperlinkUpdate(HyperlinkEvent e)
                {
                    if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                        try {
                            Desktop.getDesktop().open(file);
                        } catch (Exception ex) {
                            LauncherLog.log(Level.ERROR, ex, "Не удалось открыть ссылку");
                        }
                    }
                }
            });
            ep.setEditable(false);
            ep.setBackground(label.getBackground());

            Object[] options = new Object[] {"Открыть папку", "Ок"};

            int result = JOptionPane.showOptionDialog(null, ep, "Ошибка", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[1]);
            if(result == JOptionPane.YES_OPTION) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(file);
                }
            }

            if(exit)
                System.exit(0);
        } catch (IOException e) {
            LauncherLog.log(Level.ERROR, e, "Не удалось создать окно с информацией о ошибке.");
        }
    }

    private static Config loadConfiguration() {
        Config config = null;

        try {
            if (CONFIG_FILE.exists()) {
                config = GSON.fromJson(FileUtils.readFileToString(CONFIG_FILE), Config.class);
            }
        } catch (IOException | JsonSyntaxException ignored) {
        }

        if(config == null) {
            config = new Config();
        }

        return config;
    }

    public static void saveConfiguration() {
        if(!CONFIG_FILE.exists() || CONFIG_FILE.delete()) {
            try {
                FileUtils.writeStringToFile(CONFIG_FILE, GSON.toJson(CONFIG));
            } catch (IOException ex) {
                showAndLogError(String.format("Не удалось сохранить конфиг %s", CONFIG_FILE.getAbsolutePath()), ex);
            }
        }
    }

    public static File getLauncherFile() throws IOException {
        try {
            return new File(LauncherUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
        } catch (URISyntaxException e) {
            LauncherLog.log(Level.ERROR, e, "Не удалось получить путь до лаунчера");
        }

        throw new IOException("Не удалось получить путь до лаунчера");
    }

    public static void logInformation() {
        LauncherLog.fine("Operating System: %s (%s) version %s", System.getProperty("os.name"), System.getProperty("os.arch"), System.getProperty("os.version"));
        LauncherLog.fine("Java Version: %s, %s", System.getProperty("java.version"), System.getProperty("java.vendor"));
        LauncherLog.fine("Java VM Version: %s (%s), %s", System.getProperty("java.vm.name"), System.getProperty("java.vm.info"), System.getProperty("java.vm.vendor"));

        Runtime runtime = Runtime.getRuntime();
        long maxMemory = runtime.maxMemory();
        long totalMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        long maxMemoryMB = maxMemory / 1024L / 1024L;
        long totalMemoryMB = totalMemory / 1024L / 1024L;
        long freeMemoryMB = freeMemory / 1024L / 1024L;

        LauncherLog.fine("Memory: %s bytes (%s MB) / %s bytes (%s MB) up to %s bytes (%s MB)", freeMemory, freeMemoryMB, totalMemory, totalMemoryMB, maxMemory, maxMemoryMB);

        Joiner joiner = Joiner.on(", ").skipNulls();

        String properties = joiner.join(Iterables.transform(CONFIG.getProperties().entrySet(), new Function<Map.Entry<String,Object>, String>() {
            @Override
            public String apply(Map.Entry<String, Object> input) {
                return input.getKey() + ":" + input.getValue();
            }
        }));

        LauncherLog.fine("Properties: %s", properties);
    }

    public static HttpURLConnection createUrlConnection(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(15000);
        connection.setUseCaches(false);
        return connection;
    }

    public static String performGetRequest(URL url) throws IOException {
        HttpURLConnection connection = createUrlConnection(url);

        InputStream inputStream = null;
        try {
            inputStream = connection.getInputStream();
            return IOUtils.toString(inputStream, Charsets.UTF_8);
        } catch (IOException e) {
            IOUtils.closeQuietly(inputStream);
            inputStream = connection.getErrorStream();

            if (inputStream != null) {
                return IOUtils.toString(inputStream, Charsets.UTF_8);
            } else {
                throw e;
            }
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    public static URI getLog4jConfigLocation() {
        try {
            return LauncherUtils.class.getResource("/resources/log4j2.xml").toURI();
        } catch (Exception e) {
            showAndLogError("Не удалось загрузить конфигурацию логгера", e, true);
        }

        return null;
    }

    public static boolean checkETag(String path, String md5) throws IOException {
        URL remote = HTTPUtils.constantURL(String.format("%s/%s", "http://142408.selcdn.com/centurymine/", path));

        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) remote.openConnection();
            con.setUseCaches(false);
            con.setDefaultUseCaches(false);
            con.setRequestProperty("Cache-Control", "no-store,max-age=0,no-cache");
            con.setRequestProperty("Expires", "0");
            con.setRequestProperty("Pragma", "no-cache");
            con.setRequestProperty("If-None-Match", "\"" + md5 + "\"");

            return con.getResponseCode() == 304;
        } catch (IOException ignored) {
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }

        throw new IOException("Не удалось проверить лаунчер.");
    }

    private static final WindowsRegistry WINDOWS_REGISTRY = loadWindowsRegistry();
    private static final String FUIX_REGISTRY_PATH = "Software\\Fuix";
    private static final String CRYPT_KEY = loadCryptKey();

    private static WindowsRegistry loadWindowsRegistry() {
        return OperatingSystem.getCurrentPlatform().isWindows() ? WindowsRegistry.getInstance() : null;
    }

    private static String loadCryptKey() {
        String encryptionKey = null;

        try {
            switch (OperatingSystem.getCurrentPlatform()) {
                case WINDOWS:
                    WINDOWS_REGISTRY.createKey(HKey.HKCU, FUIX_REGISTRY_PATH);

                    encryptionKey = WINDOWS_REGISTRY.readString(HKey.HKCU, FUIX_REGISTRY_PATH, "key");
                    if (encryptionKey == null) {
                        WINDOWS_REGISTRY.writeStringValue(HKey.HKCU, FUIX_REGISTRY_PATH, "key", encryptionKey = UUID.randomUUID().toString());
                    }

                    break;
                default:
                    setProperty("key", encryptionKey = getStringProperty("key", UUID.randomUUID().toString()));
                    break;
            }
        } catch (RegistryException ex) {
            LauncherLog.log(Level.ERROR, ex, "Ошибка реестра");
        }

        return encryptionKey;
    }

    public static String getUserCredentials(String key, String defaultValue) {
        try {
            String value = OperatingSystem.getCurrentPlatform() == OperatingSystem.WINDOWS ? WINDOWS_REGISTRY.readString(HKey.HKCU, FUIX_REGISTRY_PATH, key) : getStringProperty(key, null);
            if(value == null) {
                return defaultValue;
            }

            return EncryptionUtils.decrypt(value, CRYPT_KEY);
        } catch (RegistryException ignored) {
        }

        return defaultValue;
    }

    public static void setUserCredentials(String key, String value) {
        try {
            switch (OperatingSystem.getCurrentPlatform()) {
                case WINDOWS:
                    WINDOWS_REGISTRY.writeStringValue(HKey.HKCU, FUIX_REGISTRY_PATH, key, EncryptionUtils.encrypt(value, CRYPT_KEY));
                    break;
                default:
                    setProperty(key, EncryptionUtils.encrypt(value, CRYPT_KEY));
                    break;
            }
        } catch (RegistryException ignored) {
        }
    }

    public static String getStringProperty(String key, String defaultValue) {
        return getProperty(key, defaultValue, String.class);
    }

    public static boolean getBooleanProperty(String key, boolean defaultValue) {
        return getProperty(key, defaultValue, Boolean.class);
    }

    public static int getIntProperty(String key, int defaultValue) {
        return getNumberProperty(key, defaultValue).intValue();
    }

    public static long getLongProperty(String key, long defaultValue) {
        return getNumberProperty(key, defaultValue).longValue();
    }

    public static double getDoubleProperty(String key, double defaultValue) {
        return getNumberProperty(key, defaultValue).doubleValue();
    }

    public static <T> T getProperty(String key, T defaultValue, Class<T> type) {
        return CONFIG.getProperties().containsKey(key) ? type.cast(CONFIG.getProperties().get(key)) : defaultValue;
    }

    public static Number getNumberProperty(String key, Number defaultValue) {
        return getProperty(key, defaultValue, Number.class);
    }

    public static String getLauncherProperty(String key, String defaultValue) {
        return CONFIG.getProperties().containsKey(key) ? String.valueOf(CONFIG.getProperties().get(key)) : defaultValue;
    }

    public static void setProperty(String key, Object value) {
        CONFIG.getProperties().put(key, value);
    }

    private static class Config {

        @SerializedName("properties")
        private Map<String, Object> properties;

        private Config() {
            properties = Maps.newHashMap();
        }

        public Map<String, Object> getProperties() {
            return properties;
        }
    }
}
