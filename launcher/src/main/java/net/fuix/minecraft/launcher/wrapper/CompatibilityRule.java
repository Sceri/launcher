package net.fuix.minecraft.launcher.wrapper;

import net.fuix.minecraft.launcher.util.OperatingSystem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Создано Sceri 07.01.2015.
 */
public class CompatibilityRule {

    private Action action = Action.ALLOW;
    private OSRestriction os;

    public Action getAppliedAction() {
        if (os != null && !os.isCurrentOperatingSystem()) {
            return null;
        }

        return action;
    }

    public Action getAction() {
        return action;
    }

    public OSRestriction getOs() {
        return os;
    }

    public static enum Action {
        ALLOW, DISALLOW
    }

    public class OSRestriction {
        private OperatingSystem name;
        private String version;
        private String arch;

        public OperatingSystem getName() {
            return name;
        }

        public String getVersion() {
            return version;
        }

        public String getArch() {
            return arch;
        }

        public boolean isCurrentOperatingSystem() {
            if (name != null && name != OperatingSystem.getCurrentPlatform()) {
                return false;
            }
            if (version != null) {
                try {
                    Pattern pattern = Pattern.compile(version);
                    Matcher matcher = pattern.matcher(System.getProperty("os.version"));
                    if (!matcher.matches()) {
                        return false;
                    }
                } catch (Throwable ignored) {
                }
            }
            if (arch != null) {
                try {
                    Pattern pattern = Pattern.compile(arch);
                    Matcher matcher = pattern.matcher(System.getProperty("os.arch"));
                    if (!matcher.matches()) {
                        return false;
                    }
                } catch (Throwable ignored) {
                }
            }
            return true;
        }
    }
}
