package net.fuix.minecraft.launcher.gui;

import com.sun.management.OperatingSystemMXBean;
import net.fuix.minecraft.launcher.util.LauncherUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.management.ManagementFactory;

/**
 * Created by Sceri 07.07.2016.
 */
public class Settings extends JFrame {
    private JComboBox<Integer> comboBox;
    private JCheckBox autoMemory;
    private JCheckBox savePassword;

    public Settings() {
        super("Настройки");

        setResizable(false);
        JPanel rootPanel = new JPanel(new GridLayout(3, 2));
        setContentPane(rootPanel);

        pack();
        setMinimumSize(new Dimension(400, 100));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setVisible(true);

        rootPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        rootPanel.add(new JLabel("Оперативная память (мб):"));
        rootPanel.add(comboBox = new JComboBox<Integer>());
        rootPanel.add(new JLabel("Авт. выделение памяти:"));
        rootPanel.add(autoMemory = new JCheckBox());
        rootPanel.add(new JLabel("Сохранять пароль:"));
        rootPanel.add(savePassword = new JCheckBox());

        savePassword.setSelected(LauncherUtils.getBooleanProperty("savePassword", true));
        savePassword.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LauncherUtils.setProperty("savePassword", savePassword.isSelected());
                LauncherUtils.saveConfiguration();
            }
        });

        autoMemory.setSelected(LauncherUtils.getBooleanProperty("autoMemory", true));
        autoMemory.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LauncherUtils.setProperty("autoMemory", autoMemory.isSelected());
                LauncherUtils.saveConfiguration();

                comboBox.setEnabled(!comboBox.isEnabled());
                reloadItems();
            }
        });

        reloadItems();

        comboBox.setEnabled(!autoMemory.isSelected());
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LauncherUtils.setProperty("memory", comboBox.getSelectedItem());
                LauncherUtils.saveConfiguration();
            }
        });
    }

    private void reloadItems() {
        comboBox.removeAllItems();

        int[] memoryItems = new int[] {
                256, 512, 1024, 1535, 2048, 3072, 4096, 8092, 16184
        };

        boolean is64 = System.getProperty("sun.arch.data.model").equals("64");
        int memoryLimit = (int) (((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / 1024 / 1024);
        int closestMemory = autoMemory.isSelected() ? getClosestMemory(memoryItems, is64 ? memoryLimit / 2 : 1024) : 1024;
        for(int memoryItem : memoryItems) {
            if(memoryItem <= memoryLimit) {
                comboBox.addItem(memoryItem);

                if(autoMemory.isSelected()) {
                    if(memoryItem == closestMemory) {
                        comboBox.setSelectedItem(memoryItem);
                        LauncherUtils.setProperty("memory", memoryItem);
                        LauncherUtils.saveConfiguration();
                    }
                } else {
                    if(memoryItem == LauncherUtils.getIntProperty("memory", 1024)) {
                        comboBox.setSelectedItem(memoryItem);
                    }
                }
            }
        }
    }

    private int getClosestMemory(int[] memoryItems, int memory) {
        int closest = memoryItems[0];
        int distance = Math.abs(closest - memory);
        for (int memoryItem : memoryItems) {
            int distanceI = Math.abs(memoryItem - memory);
            if (distance > distanceI) {
                closest = memoryItem;
                distance = distanceI;
            }
        }

        return closest;
    }

}
