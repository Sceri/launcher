package net.fuix.minecraft.launcher.gui.component;

import net.fuix.minecraft.launcher.gui.MainPane;
import net.fuix.minecraft.launcher.util.ImageUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class LButton extends JButton {
    private static final BufferedImage TEXTURE = ImageUtils.getImage("button");
    private final String text;

    public LButton(String text, ActionListener listener) {
        super("");

        this.text = text;

        setOpaque(false);
        setFocusable(false);
        setFocusPainted(false);
        setBorderPainted(false);
        setContentAreaFilled(false);
        setFont(MainPane.FONT);
        addActionListener(listener);
        setForeground(Color.WHITE);
        setHorizontalAlignment(SwingConstants.CENTER);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        ButtonModel bml = getModel();

        int w = TEXTURE.getWidth();
        int h = TEXTURE.getHeight() / 3;

        int y = h * (bml.isRollover() ? bml.isPressed() ? 2 : 1 : 0);
        g2d.drawImage(TEXTURE, 0, 0, getWidth(), getHeight(), 0, y, w, y + h, null);

        int textX = w / 2 - (g2d.getFontMetrics().stringWidth(text) / 2);
        int textY = 17;

        ImageUtils.paintString(text, textX, textY, g, bml.isEnabled() ? bml.isRollover() ? new Color(255, 212, 0) : getForeground() : new Color(220, 220, 220), new Color(50, 50, 50));

        super.paintComponent(g);
    }
}