package net.fuix.minecraft.launcher;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mojang.authlib.exceptions.RequestException;
import com.mojang.authlib.utils.HTTPUtils;
import net.fuix.minecraft.launcher.gui.MainPane;
import net.fuix.minecraft.launcher.util.*;
import net.fuix.minecraft.launcher.wrapper.WrapperProfile;
import net.fuix.minecraft.launcher.wrapper.WrapperProfile.WrapperProfileAdapter;
import net.fuix.minecraft.launcher.wrapper.WrapperProfilesResponse;
import net.fuix.minecraft.launcher.wrapper.WrapperResource.WrapperResourceAdapter;
import net.minecraft.launchwrapper.*;
import net.minecraft.launchwrapper.exceptions.WrapperException;
import sun.misc.URLClassPath;

import javax.swing.*;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarFile;

/**
 * Created by Sceri 30.06.2016.
 */
public class Launcher implements ILauncher {
    public static final URL WRAPPER_PROFILE_URL = HTTPUtils.constantURL(HTTPUtils.BASE_URL + "wrapperProfile");

    private final MainPane pane;
    private final JFrame frame;

    private List<WrapperProfile> wrapperProfiles = Lists.newArrayList();
    private WrapperProfile selectedProfile;

    private final UserInfo userInfo;

    public Launcher(JFrame frame) {
        this.frame = frame;

        frame.setTitle("CenturyMine Launcher");

        this.userInfo = new UserInfo();

        LauncherUtils.logInformation();

        setSelectedProfile(0);
        loadProfiles();

        frame.setUndecorated(true);
        frame.setResizable(false);
        frame.setIconImage(new ImageIcon(ImageUtils.getImage("favicon")).getImage());
        frame.setContentPane(pane = new MainPane(this));
        frame.pack();
        frame.setMinimumSize(pane.getPreferredSize());
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {
        }

        new Launcher(new JFrame());
    }

    public JFrame getFrame() {
        return frame;
    }

    public List<WrapperProfile> getWrapperProfiles() {
        return wrapperProfiles;
    }

    public WrapperProfile getSelectedProfile() {
        return selectedProfile;
    }

    @Override
    public IUserInfo getUserInfo() {
        return userInfo;
    }

    @Override
    public void openLauncher() {
        System.exit(0);
    }

    @Override
    public void hideLauncher() {
        frame.dispose();
    }

    @Override
    public void check(LaunchClassLoader classLoader) throws WrapperException {
        URLClassPath urlClassPath = ReflectionUtils.getPrivateValue(URLClassLoader.class, classLoader, "ucp");
        ArrayList<Object> loaders = ReflectionUtils.getPrivateValue(URLClassPath.class, urlClassPath, "loaders");
        Class<?> jarLoader = ReflectionUtils.getClass(classLoader, "sun.misc.URLClassPath$JarLoader");

        ArrayList<String> urls = Lists.newArrayList(Iterables.filter(Iterables.transform(classLoader.getSources(), new Function<URL, String>() {
            @Override
            public String apply(URL url) {
                try {
                    return new File(url.toURI()).getAbsolutePath();
                } catch (URISyntaxException ignored) {
                }

                return null;
            }
        }), Predicates.notNull()));

        boolean modified = false;

        for (Object loader : loaders) {
            if (loader.getClass().isAssignableFrom(jarLoader)) {
                JarFile jarFile = ReflectionUtils.getRawPrivateValue(jarLoader, loader, "jar");
                if (jarFile != null) {
                    if (!urls.contains(jarFile.getName())) {
                        modified = true;
                        break;
                    }
                }
            }
        }

        if (!modified) {
            RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
            List<String> arguments = runtimeMxBean.getInputArguments();

            for (String argument : arguments) {
                if (argument.startsWith("-Xbootclasspath")) {
                    modified = true;
                    break;
                }
            }
        }

        if (modified) {
            System.exit(0);
        }
    }

    @Override
    public String getLauncherProperty(String s) {
        return LauncherUtils.getLauncherProperty(s, null);
    }

    public static final Gson WRAPPER_GSON = new GsonBuilder().
            registerTypeHierarchyAdapter(IWrapperProfile.class, new WrapperProfileAdapter()).
            registerTypeHierarchyAdapter(IWrapperResource.class, new WrapperResourceAdapter()).
            registerTypeAdapter(Multimap.class, new MultimapAdapter()).
            create();

    private void loadProfiles() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    WrapperProfilesResponse response = HTTPUtils.makeRequest(WRAPPER_GSON, WRAPPER_PROFILE_URL, null, WrapperProfilesResponse.class);
                    wrapperProfiles.clear();
                    wrapperProfiles.addAll(response.getProfiles());
                    setSelectedProfile(LauncherUtils.getIntProperty("selectedProfile", 0));
                } catch (RequestException e) {
                    LauncherUtils.showAndLogError(e.getMessage(), e, true);
                }
            }
        }).start();
    }

    public void setSelectedProfile(WrapperProfile selectedProfile) {
        this.selectedProfile = selectedProfile;
    }

    public void setSelectedProfile(int selectedProfile) {
        setSelectedProfile(wrapperProfiles.size() == 0 ? new WrapperProfile("Подождите") : selectedProfile < wrapperProfiles.size() && selectedProfile >= 0 ? wrapperProfiles.get(selectedProfile) : wrapperProfiles.get(0));
    }

}
