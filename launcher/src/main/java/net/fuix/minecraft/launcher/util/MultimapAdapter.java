package net.fuix.minecraft.launcher.util;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.*;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;

/**
 * Создано Sceri 11.04.2015.
 */
public class MultimapAdapter<K, V> implements JsonDeserializer<Multimap<K, V>>, JsonSerializer<Multimap<K, V>> {

    @Override
    @SuppressWarnings("unchecked")
    public Multimap<K, V> deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        final Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();

        Multimap<K, V> result = HashMultimap.create();

        if (json instanceof JsonArray) {
            for (JsonElement element : (JsonArray) json) {
                if (element instanceof JsonObject) {
                    JsonObject object = (JsonObject) element;

                    K key = context.deserialize(object.get("name"), typeArguments[0]);
                    for (JsonElement valueElement : (JsonArray) object.get("value")) {
                        valueElement.getAsJsonObject().add("key", object.get("name"));
                        V value = context.deserialize(valueElement, typeArguments[1]);
                        result.put(key, value);
                    }
                }
            }
        }

        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public JsonElement serialize(Multimap<K, V> src, Type type, JsonSerializationContext context) {
        final Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
        final TypeToken<Collection<V>> valueToken = new TypeToken<Collection<V>>() {
        }.where(new TypeParameter<V>() {
        }, (TypeToken<V>) TypeToken.of(typeArguments[1]));

        JsonArray result = new JsonArray();

        for (Map.Entry<K, Collection<V>> entry : src.asMap().entrySet()) {
            JsonObject object = new JsonObject();

            object.add("name", context.serialize(entry.getKey(), typeArguments[0]));
            object.add("value", context.serialize(entry.getValue(), valueToken.getType()));

            result.add(object);
        }

        return result;
    }
}
