package net.fuix.minecraft.launcher.gui.component;

import net.fuix.minecraft.launcher.gui.MainPane;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.image.BufferedImage;

public class LPassword extends JPasswordField {

    public LPassword() {
        setLayout(null);
        setOpaque(false);
        setEchoChar('*');
        setFont(MainPane.FONT);
        setBorder(new EmptyBorder(5, 10, 5, 42));
        setForeground(MainPane.ORANGE);
        setCaretColor(MainPane.ORANGE);
    }
}