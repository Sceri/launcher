package net.fuix.minecraft.launcher.util;

import net.minecraft.launchwrapper.IUserInfo;

/**
 * Created by Sceri 06.07.2016.
 */
public class UserInfo implements IUserInfo {
    private String name;
    private String password;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
