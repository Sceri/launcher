package net.fuix.minecraft.launcher.gui;

import com.google.common.base.Throwables;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import com.mojang.authlib.auth.AuthenticationManager;
import com.mojang.authlib.exceptions.auth.AuthenticationException;
import net.fuix.minecraft.launcher.Launcher;
import net.fuix.minecraft.launcher.download.DownloadException;
import net.fuix.minecraft.launcher.gui.component.*;
import net.fuix.minecraft.launcher.util.*;
import net.minecraft.launchwrapper.exceptions.WrapperClientException;
import net.minecraft.launchwrapper.exceptions.WrapperException;
import org.apache.commons.codec.binary.Hex;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Sceri 30.06.2016.
 */
public class MainPane extends JPanel implements HyperlinkListener, FocusListener, MouseListener, MouseMotionListener {
    public static final Font FONT = getTahomaFont();
    public static final Color ORANGE = new Color(247, 77, 47);
    private static final BufferedImage BACKGROUND = ImageUtils.getImage("background");
    private final Launcher launcher;

    private final LTextField login;
    private final LPassword password;
    private final LComboBox comboBox;
    private final LButton loginButton;

    private TextPane currentPane;
    private TextPane authPane;
    private TextPane launchPane;
    private DownloadPane downloadPane;

    private int x, y;

    public MainPane(Launcher launcher) {
        super(null);

        this.launcher = launcher;

        int width = BACKGROUND.getWidth();
        int height = BACKGROUND.getHeight();

        setOpaque(false);
        setPreferredSize(new Dimension(width, height));

        addMouseListener(this);
        addMouseMotionListener(this);

        final JTextPane newsView = new JTextPane();
        newsView.setBorder(null);
        newsView.setOpaque(false);
        newsView.setEditable(false);
        newsView.setFocusable(false);
        newsView.setContentType("text/html");
        newsView.addHyperlinkListener(this);

        JScrollPane news = new JScrollPane(newsView);
        news.setBorder(null);
        news.setOpaque(false);
        news.getViewport().setOpaque(false);
        news.setBounds(410, 40, 490, height - 20);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    newsView.setPage(new URL("http://web-server.fuix.net/web_launcher_news"));
                } catch (IOException e) {
                    newsView.setText("Ошибка загрузки: " + e);
                }
            }
        }).start();

        login = new LTextField();
        login.setBounds(133, 203, 193, 29);
        login.setText(LauncherUtils.getUserCredentials("username", "Логин..."));
        login.addFocusListener(this);

        password = new LPassword();
        password.setBounds(133, 234, 193, 29);
        password.setText(LauncherUtils.getUserCredentials("password", "Пароль..."));
        password.addFocusListener(this);

        comboBox = new LComboBox(this, 266);
        comboBox.setBounds(107, 266, 226, 28);

        loginButton = new LButton("Войти в игру", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                disableComponents();
                startRepaintThread();

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        startLaunch();
                    }
                });
                thread.setName("Main");
                thread.start();
            }
        });

        loginButton.setBounds(107, 294, 226, 28);
        getLauncher().getFrame().getRootPane().setDefaultButton(loginButton);

        Font font = new Font("Arial", Font.PLAIN, 10);

        String string = "Регистрация";
        JLabel label = new JLabel(string);
        label.setForeground(Color.WHITE);
        label.setFont(font);
        label.setBounds(90, 335, getFontMetrics(font).stringWidth(string), getFontMetrics(font).getHeight());

        final JLabel regLabel = label;
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                regLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
                regLabel.setForeground(MainPane.ORANGE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                regLabel.setCursor(Cursor.getDefaultCursor());
                regLabel.setForeground(Color.WHITE);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    OperatingSystem.openLink(new URI("http://centurymine.ru/reg"));
                } catch (URISyntaxException ignored) {
                }
            }
        });

        add(label);

        string = "Забыли пароль?";
        label = new JLabel(string);
        label.setForeground(Color.WHITE);
        label.setFont(font);
        label.setBounds(180, 335, getFontMetrics(font).stringWidth(string), getFontMetrics(font).getHeight());
        final JLabel forgotLabel = label;
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                forgotLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
                forgotLabel.setForeground(MainPane.ORANGE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                forgotLabel.setCursor(Cursor.getDefaultCursor());
                forgotLabel.setForeground(Color.WHITE);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    OperatingSystem.openLink(new URI("http://centurymine.ru/rememberpassword"));
                } catch (URISyntaxException ignored) {
                }
            }
        });

        add(label);

        string = "Настройки";
        label = new JLabel(string);
        label.setForeground(Color.WHITE);
        label.setFont(font);
        label.setBounds(290, 335, getFontMetrics(font).stringWidth(string), getFontMetrics(font).getHeight());
        final JLabel settingsLabel = label;
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                settingsLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
                settingsLabel.setForeground(MainPane.ORANGE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                settingsLabel.setCursor(Cursor.getDefaultCursor());
                settingsLabel.setForeground(Color.WHITE);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                new Settings();
            }
        });

        add(label);

        LTitleButton titleButton = new LTitleButton("hide", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getLauncher().getFrame().setExtendedState(Frame.ICONIFIED);
            }
        });
        titleButton.setBounds(830, 0, 32, 24);
        add(titleButton);

        titleButton = new LTitleButton("close", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        titleButton.setBounds(862, 0, 32, 24);
        add(titleButton);

        add(comboBox);
        add(loginButton);
        add(login);
        add(password);
        add(news);
    }

    private ScheduledExecutorService repaintExecutor;
    private void startRepaintThread() {
        repaintExecutor = Executors.newScheduledThreadPool(1);
        repaintExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if(authPane != null) {
                    authPane.incTicker();
                }
                if(downloadPane != null) {
                    downloadPane.incTicker();
                }
                if(launchPane != null) {
                    launchPane.incTicker();
                }
                repaint();
            }
        }, 0L, 200L, TimeUnit.MILLISECONDS);
    }

    private void stopRepaint() {
        if(repaintExecutor != null && !repaintExecutor.isShutdown()) {
            repaintExecutor.shutdown();
        }
    }

    public void error(TextPane textPane, String error) {
        textPane.setText(error);
        textPane.stop();

        repaint();
        pause();
        remove(textPane);
        stopRepaint();
        enableComponents();
        repaint();
    }

    private void pause() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ignored) {
        }
    }

    private void disableComponents() {
        login.setEnabled(false);
        password.setEnabled(false);
        comboBox.setEnabled(false);
        loginButton.setEnabled(false);
    }

    private void enableComponents() {
        login.setEnabled(true);
        password.setEnabled(true);
        comboBox.setEnabled(true);
        loginButton.setEnabled(true);
    }

    public void startLaunch() {
        try {
            startAuthentication();
            startDownload();
            launchClient();
        } catch (AuthenticationException | DownloadException e) {
            error(currentPane, e.getMessage());
        } catch (WrapperException e) {
            try {
                Throwables.propagateIfInstanceOf(e, WrapperClientException.class);
            } catch (WrapperClientException e1) {
                LauncherUtils.showAndLogError(String.format("Не удалось запустить %s клиент.", launcher.getSelectedProfile().getName()), e, true);
                error(currentPane, "Не удалось запустить клиент");
                return;
            }

            error(currentPane, e.getMessage());
        }
    }

    public void startAuthentication() throws AuthenticationException {
        add(currentPane = authPane = new TextPane("Авторизация", new Font("Arial", Font.BOLD, 14), 23, 385, 384, 165));

        String username = login.getText();
        String pass = password.getText();

        AuthenticationManager manager = AuthenticationManager.getInstance();

        manager.auth(username, pass, null);
        LauncherUtils.setUserCredentials("username", username);
        if (LauncherUtils.getBooleanProperty("savePassword", true)) {
            LauncherUtils.setUserCredentials("password", pass);
        }

        UserInfo userInfo = (UserInfo) getLauncher().getUserInfo();
        userInfo.setName(username);
        userInfo.setPassword(pass);

        LauncherUtils.saveConfiguration();
    }

    public void startDownload() throws WrapperException, DownloadException {
        remove(authPane); authPane = null;
        add(currentPane = downloadPane = new DownloadPane(this, 23, 385, 384, 165));

        RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
        java.util.List<String> arguments = runtimeMxBean.getInputArguments();

        boolean founded = false;

        for (String argument : arguments) {
            if (argument.equals("-XX:+DisableAttachMechanism")) {
                founded = true;
                break;
            }
        }

        if(!founded) {
            throw new DownloadException("Запустите лаунчер через загрузчик.");
        }

        try {
            String md5 = Hex.encodeHexString(Hashing.md5().hashBytes(Files.toByteArray(LauncherUtils.getLauncherFile())).asBytes());
            if (!LauncherUtils.checkETag("libraries/net/fuix/minecraft/launcher/latest_promoted/launcher-latest_promoted.jar", md5)) {
                throw new DownloadException("Перезапустите лаунчер");
            }
        } catch (IOException e) {
            throw new DownloadException("Не удалось проверить лаунчер");
        }

        downloadPane.download(getLauncher().getSelectedProfile());
    }

    public void launchClient() throws WrapperException {
        remove(downloadPane); downloadPane = null;
        add(currentPane = launchPane = new TextPane("Запуск клиента", new Font("Arial", Font.BOLD, 16), 23, 385, 384, 165));

        LauncherLog.destroy();
        launcher.getSelectedProfile().launch(launcher, true);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(BACKGROUND, 0, 0, null);

        super.paintComponent(g);
    }

    private static Font getTahomaFont() {
        return new Font("Arial", Font.BOLD, 12);
    }

    public Launcher getLauncher() {
        return launcher;
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (!HyperlinkEvent.EventType.ACTIVATED.equals(e.getEventType())) {
            return;
        }
        try {
            Desktop.getDesktop().browse(e.getURL().toURI());
        } catch (IOException | URISyntaxException e1) {
            System.err.println("Error opening link: " + e1);
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        if (e.getSource() instanceof LTextField) {
            LTextField textfield = (LTextField) e.getSource();
            if (!"Логин...".equals(textfield.getText())) {
                return;
            }
            textfield.setText("");
            return;
        }
        LPassword password = (LPassword) e.getSource();
        if (!"Пароль...".equals(new String(password.getPassword()))) {
            return;
        }
        password.setText("");
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (e.getSource() instanceof LTextField) {
            LTextField textfield = (LTextField) e.getSource();
            if (!textfield.getText().isEmpty()) {
                return;
            }
            textfield.setText("Логин...");
            return;
        }
        LPassword password = (LPassword) e.getSource();
        if (password.getPassword().length != 0) {
            return;
        }
        password.setText("Пароль...");
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        getLauncher().getFrame().setLocation(e.getX() + getLauncher().getFrame().getX() - x, e.getY() + getLauncher().getFrame().getY() - y);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        x = e.getX();
        y = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }
}
