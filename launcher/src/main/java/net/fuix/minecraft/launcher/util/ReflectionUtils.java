package net.fuix.minecraft.launcher.util;

import net.minecraft.launchwrapper.exceptions.WrapperException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Создано Sceri 11.12.2015.
 */
public class ReflectionUtils {

    public static Field findField(Class<?> clazz, String... fieldNames) throws WrapperException {
        Exception failed = null;
        for (String fieldName : fieldNames) {
            try {
                Field f = clazz.getDeclaredField(fieldName);
                f.setAccessible(true);
                return f;
            } catch (Exception e) {
                failed = e;
            }
        }

        throw new WrapperException(failed);
    }

    @SuppressWarnings("unchecked")
    public static <T, E> T getPrivateValue(Class<? super E> classToAccess, E instance, int fieldIndex) throws WrapperException {
        try {
            Field f = classToAccess.getDeclaredFields()[fieldIndex];
            f.setAccessible(true);
            return (T) f.get(instance);
        } catch (Exception e) {
            throw new WrapperException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T, E> T getPrivateValue(Class<? super E> classToAccess, E instance, String... fieldNames) throws WrapperException {
        try {
            return (T) findField(classToAccess, fieldNames).get(instance);
        } catch (Exception e) {
            throw new WrapperException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T getRawPrivateValue(Class<?> classToAccess, Object instance, String... fieldNames) throws WrapperException {
        try {
            return (T) findField(classToAccess, fieldNames).get(instance);
        } catch (Exception e) {
            throw new WrapperException(e);
        }
    }

    public static <T, E> void setPrivateValue(Class<? super T> classToAccess, T instance, E value, int fieldIndex) throws WrapperException {
        try {
            Field f = classToAccess.getDeclaredFields()[fieldIndex];
            f.setAccessible(true);
            f.set(instance, value);
        } catch (Exception e) {
            throw new WrapperException(e);
        }
    }

    public static <T, E> void setPrivateValue(Class<? super T> classToAccess, T instance, E value, String... fieldNames) throws WrapperException {
        try {
            findField(classToAccess, fieldNames).set(instance, value);
        } catch (Exception e) {
            throw new WrapperException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static Class<? super Object> getClass(ClassLoader loader, String... classNames) throws WrapperException {
        Exception err = null;
        for (String className : classNames) {
            try {
                return (Class<? super Object>) Class.forName(className, false, loader);
            } catch (Exception e) {
                err = e;
            }
        }

        throw new WrapperException(err);
    }

    public static <E> Method findMethod(Class<? super E> clazz, E instance, String[] methodNames, Class<?>... methodTypes) throws WrapperException {
        Exception failed = null;
        for (String methodName : methodNames) {
            try {
                Method m = clazz.getDeclaredMethod(methodName, methodTypes);
                m.setAccessible(true);
                return m;
            } catch (Exception e) {
                failed = e;
            }
        }
        throw new WrapperException(failed);
    }

}
