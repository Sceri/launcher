package net.fuix.minecraft.launcher.download;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Создано Sceri 09.01.2015.
 */
public class MonitoringInputStream extends FilterInputStream {
    private final DownloadTask downloadTask;

    public MonitoringInputStream(InputStream in, DownloadTask downloadTask) {
        super(in);
        this.downloadTask = downloadTask;
    }

    @Override
    public int read() throws IOException {
        int result = in.read();
        if (result >= 0) {
            downloadTask.addProgress(1L);
        }

        return result;
    }

    @Override
    public int read(byte[] buffer) throws IOException {
        int size = in.read(buffer);
        if (size >= 0) {
            downloadTask.addProgress(size);
        }

        return size;
    }

    @Override
    public int read(byte[] buffer, int off, int len) throws IOException {
        int size = in.read(buffer, off, len);
        if (size > 0) {
            downloadTask.addProgress(size);
        }

        return size;
    }

    @Override
    public long skip(long size) throws IOException {
        long skipped = super.skip(size);

        if (skipped > 0L) {
            downloadTask.addProgress(skipped);
        }

        return skipped;
    }
}
