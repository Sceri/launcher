package net.fuix.minecraft.launcher.util;

import com.google.common.base.Objects;
import net.fuix.minecraft.launcher.wrapper.CheckType;
import net.minecraft.launchwrapper.IWrapperProfile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.SystemUtils.FILE_SEPARATOR;
import static org.apache.commons.lang3.SystemUtils.getUserHome;

/**
 * Создано Sceri 30.12.2014.
 */
public class DirectoryUtils {
    public static final File MAIN_DIRECTORY = getMainDirectory();
    public static final File CLIENTS_DIRECTORY = getClientsDirectory();

    public static String replaceTokens(String path, IWrapperProfile profile) {
        checkNotNull(path);

        String result = path;
        if (path.contains("%MAIN_DIR%")) {
            result = result.replace("%MAIN_DIR%", MAIN_DIRECTORY.getAbsolutePath());
        }
        if (path.contains("%CLIENT_DIR%")) {
            checkNotNull(profile);
            result = result.replace("%CLIENT_DIR%", getClientDirectory(profile).getAbsolutePath());
        }

        return result;
    }

    public static File getClientDirectory(IWrapperProfile profile) {
        return new File(CLIENTS_DIRECTORY, profile.getName());
    }

    private static File getMainDirectory() {
        OperatingSystem os = OperatingSystem.getCurrentPlatform();

        File directory = os.isLinux() ?
                new File(getUserHome(), ".CenturyMine" + FILE_SEPARATOR) : os.isWindows() ?
                new File(Objects.firstNonNull(new File(System.getenv("APPDATA")), getUserHome()), ".CenturyMine" + FILE_SEPARATOR) : os.isMac() ?
                new File(getUserHome(), "Library/Application Support/" + "CenturyMine" + FILE_SEPARATOR) :
                new File(getUserHome(), "CenturyMine" + FILE_SEPARATOR);

        try {
            FileUtils.forceMkdir(directory);
        } catch (IOException ignored) {
        }

        System.setProperty("fuix.root", directory.getAbsolutePath());

        return directory;
    }

    private static File getClientsDirectory() {
        File directory = new File(MAIN_DIRECTORY, "Clients");

        try {
            FileUtils.forceMkdir(directory);
        } catch (IOException ignored) {
        }

        return directory;
    }

    public static void unzip(File sourceFile, File targetFolder) throws IOException {
        unzip(sourceFile, targetFolder, CheckType.DISABLED);
    }

    public static void unzip(File sourceFile, File targetFolder, CheckType checkType) throws IOException {
        checkNotNull(sourceFile, "Исходный файл {%s} должен существовать", sourceFile.getName());
        checkNotNull(targetFolder, "Директория {%s} должна существовать", targetFolder.getName());

        ZipFile zipFile = new ZipFile(sourceFile);

        Enumeration enumeration = zipFile.entries();
        while (enumeration.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) enumeration.nextElement();

            String path = zipEntry.getName();
            if(FilenameUtils.getExtension(path).equals("jnilib")) {
                path = path.replace("jnilib", "dylib");
            }

            File entryFile = new File(targetFolder, path);
            if (!entryFile.getAbsolutePath().contains("META-INF")) {
                if (checkType != CheckType.DISABLED) {
                    boolean exists = entryFile.exists();
                    if (checkType == CheckType.LIGHT ? exists : exists && (entryFile.isDirectory() || zipEntry.getSize() == entryFile.length())) {
                        continue;
                    }
                }

                if (zipEntry.isDirectory()) {
                    FileUtils.forceMkdir(entryFile);
                } else {
                    FileUtils.copyInputStreamToFile(zipFile.getInputStream(zipEntry), entryFile);
                }
            }
        }
    }
}
