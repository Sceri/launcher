package net.fuix.minecraft.launcher.download;

/**
 * Создано Sceri 09.01.2015.
 */
public interface IDownloadListener {

    public void onDownloadProgressChanged(Downloader downloader);

    public void onDownloadFinished(Downloader downloader) throws DownloadException;

}
