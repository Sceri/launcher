package net.fuix.minecraft.launcher.download;

import com.google.common.collect.Queues;

import java.util.*;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Создано Sceri 09.01.2015.
 */
public class Downloader {
    private static final int MAX_ATTEMPTS_PER_FILE = 3;
    private static final int ASSUMED_AVERAGE_FILE_SIZE = 5242880;

    private final Queue<DownloadTask> remainingFiles = Queues.newConcurrentLinkedQueue();
    private final List<DownloadTask> allFiles = Collections.synchronizedList(new ArrayList<DownloadTask>());
    private final List<DownloadTask> failures = Collections.synchronizedList(new ArrayList<DownloadTask>());
    private final List<DownloadTask> successful = Collections.synchronizedList(new ArrayList<DownloadTask>());

    private final List<IDownloadListener> downloadListeners;

    public Downloader(List<IDownloadListener> downloadListeners) {
        this.downloadListeners = downloadListeners;
    }

    public void addDownloadTask(DownloadTask task) {
        allFiles.add(task);
        remainingFiles.add(task);

        if (task.getExpectedSize() == 0L) {
            task.setExpectedSize(ASSUMED_AVERAGE_FILE_SIZE);
        }

        task.setDownloader(this);
    }

    public void addDownloadTasks(Collection<DownloadTask> tasks) {
        for (DownloadTask task : tasks) {
            addDownloadTask(task);
        }
    }

    public void startDownloading(ThreadPoolExecutor executorService) throws InterruptedException, DownloadException {
        int threads = executorService.getMaximumPoolSize();
        for (int i = 0; i < threads; i++) {
            executorService.submit(new Runnable() {
                public void run() {
                    download();
                }
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        for (IDownloadListener listener : downloadListeners) {
            listener.onDownloadFinished(this);
        }
    }

    public void download() {
        DownloadTask task;
        while ((task = remainingFiles.poll()) != null) {
            if (task.getStartTime() == 0L) {
                task.setStartTime(System.currentTimeMillis());
            }
            if (task.getAttempts() <= MAX_ATTEMPTS_PER_FILE) {
                try {
                    task.download();
                    successful.add(task);
                    task.setEndTime(System.currentTimeMillis());
                    task.setCurrent(task.getTotal());
                } catch (DownloadException ex) {
                    task.setLastException(ex);
                    remainingFiles.add(task);
                }
            } else {
                failures.add(task);
            }
        }
    }

    public void updateProgress() {
        for (IDownloadListener listener : downloadListeners) {
            listener.onDownloadProgressChanged(this);
        }
    }

    public boolean isComplete() {
        return remainingFiles.isEmpty();
    }

    public List<DownloadTask> getSuccessful() {
        return successful;
    }

    public List<DownloadTask> getAllFiles() {
        return allFiles;
    }

    public List<DownloadTask> getFailures() {
        return failures;
    }

    public int getRemainingFiles() {
        return remainingFiles.size();
    }
}
