package net.fuix.minecraft.launcher.wrapper;

import com.google.gson.annotations.SerializedName;
import com.mojang.authlib.Response;

import java.util.List;

/**
 * Создано Sceri 08.01.2015.
 */
public class WrapperProfilesResponse extends Response {

    @SerializedName("wrapperProfiles")
    private final List<WrapperProfile> wrapperProfiles;

    public WrapperProfilesResponse(List<WrapperProfile> profiles) {
        this.wrapperProfiles = profiles;
    }

    public List<WrapperProfile> getProfiles() {
        return wrapperProfiles;
    }

}
