package net.fuix.minecraft.launcher.gui.component;

import net.fuix.minecraft.launcher.gui.MainPane;
import net.fuix.minecraft.launcher.util.ImageUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class LTitleButton extends JButton {
    private final BufferedImage texture;

    public LTitleButton(String iconName, ActionListener listener) {
        this.texture = ImageUtils.getImage(iconName);

        setOpaque(false);
        setFocusable(false);
        setFocusPainted(false);
        setBorderPainted(false);
        setContentAreaFilled(false);
        setFont(MainPane.FONT);
        addActionListener(listener);
        setForeground(Color.DARK_GRAY);
        setHorizontalAlignment(SwingConstants.CENTER);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        ButtonModel bml = getModel();

        int h = texture.getHeight() / 2, w = texture.getWidth();
        int y = h * (bml.isRollover() ? 1 : 0);

        g2d.drawImage(texture, 0, 0, getWidth(), getHeight(), 0, y, w, y + h, null);
    }
}