package net.fuix.minecraft.launcher.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;

/**
 * Создано Sceri 10.07.2015.
 */
public class LauncherLog {

    public static LauncherLog log = new LauncherLog();
    private static boolean configured;

    private LoggerContext context;
    private Logger cobwebLog;

    private static void configureLogging() {
        log.context = (LoggerContext) LogManager.getContext(LauncherLog.class.getClassLoader(), false, LauncherUtils.getLog4jConfigLocation());
        log.cobwebLog = log.context.getLogger("Launcher");
        configured = true;
    }

    public static void destroy() {
        log.context.stop();
        configured = false;
    }

    public static void log(String targetLog, Level level, String format, Object... data) {
        LogManager.getLogger(targetLog).log(level, String.format(format, data));
    }

    public static void log(Level level, String format, Object... data) {
        if (!configured) {
            configureLogging();
        }
        log.cobwebLog.log(level, String.format(format, data));
    }

    public static void log(String targetLog, Level level, Throwable ex, String format, Object... data) {
        LogManager.getLogger(targetLog).log(level, String.format(format, data), ex);
    }

    public static void log(Level level, Throwable ex, String format, Object... data) {
        if (!configured) {
            configureLogging();
        }
        log.cobwebLog.log(level, String.format(format, data), ex);
    }

    public static void severe(String format, Object... data) {
        log(Level.ERROR, format, data);
    }

    public static void warning(String format, Object... data) {
        log(Level.WARN, format, data);
    }

    public static void info(String format, Object... data) {
        log(Level.INFO, format, data);
    }

    public static void fine(String format, Object... data) {
        log(Level.DEBUG, format, data);
    }

    public static void finer(String format, Object... data) {
        log(Level.TRACE, format, data);
    }

    public Logger getLogger() {
        return cobwebLog;
    }

}
