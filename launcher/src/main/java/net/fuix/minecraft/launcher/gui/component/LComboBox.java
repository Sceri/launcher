package net.fuix.minecraft.launcher.gui.component;

import net.fuix.minecraft.launcher.Launcher;
import net.fuix.minecraft.launcher.gui.MainPane;
import net.fuix.minecraft.launcher.util.ImageUtils;
import net.fuix.minecraft.launcher.util.LauncherUtils;
import net.fuix.minecraft.launcher.wrapper.WrapperProfile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.List;

public class LComboBox extends JComponent implements MouseListener, MouseMotionListener {
    private static final BufferedImage TEXTURE = ImageUtils.getImage("combobox");

    private final MainPane mainPane;
    private final Launcher launcher;
    private final int startY;

    private boolean rollover;
    private boolean pressed;

    private int mouseY;

    public LComboBox(final MainPane mainPane, int startY) {
        this.mainPane = mainPane;
        this.launcher = mainPane.getLauncher();
        this.startY = startY;

        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() != MouseEvent.BUTTON1 || !isEnabled()) {
            return;
        }

        List<WrapperProfile> profiles = launcher.getWrapperProfiles();

        int el = profiles.size() * 24;
        if (pressed) {
            int n = mouseY / 24;

            if (n < profiles.size()) {
                launcher.setSelectedProfile(n);
                LauncherUtils.setProperty("selectedProfile", n);
                LauncherUtils.saveConfiguration();
            }

            setSize(getWidth(), 28);
            setLocation(getX(), startY);
            rollover = (mouseY -= el) >= 0;
        } else {
            setSize(getWidth(), el + 28);
            setLocation(getX(), startY - el);
            mouseY += el;
        }


        pressed = !pressed;
        repaint();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        rollover = true;
        repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        rollover = false;
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseY = e.getY();
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.WHITE);

        int th = TEXTURE.getHeight(), tw = TEXTURE.getWidth(), w = getWidth(), h = getHeight();

        int y = 28 * (pressed ? 2 : rollover ? 1 : 0);
        g2d.drawImage(TEXTURE, 0, h - 28, w, h, 0, y, tw, y + 28, null);
        g2d.setFont(MainPane.FONT);

        int textX = tw / 2 - (g.getFontMetrics().stringWidth(launcher.getSelectedProfile().getName()) / 2);
        int textY = h - 10;

        ImageUtils.paintString(launcher.getSelectedProfile().getName(), textX, textY, g, isEnabled() ? rollover && !pressed ? new Color(255, 212, 0) : Color.WHITE : new Color(220, 220, 220), new Color(50, 50, 50));
        if (pressed) {
            for (int i = 0; i < launcher.getWrapperProfiles().size(); i++) {
                int fy = i * 24, sy = fy + 24;
                int toffset = 84 + (i == 0 ? 0 : i == /*launcher.getWrapperProfiles().size()*/ 3 - 1 ? 2 : 1) * 24;
                g2d.drawImage(TEXTURE, 0, fy, w, sy, 0, toffset, tw, toffset + 24, null);
                if (rollover && mouseY >= fy && mouseY < sy) {
                    g2d.drawImage(TEXTURE, 0, fy, w, sy, 0, th - 24, tw, th, null);
                }
                g2d.setFont(MainPane.FONT);

                String profileName = launcher.getWrapperProfiles().get(i).getName();

                int profileX = tw / 2 - (g.getFontMetrics().stringWidth(profileName) / 2);
                int profileY = fy + 18;

                ImageUtils.paintString(profileName, profileX, profileY, g, Color.WHITE, new Color(50, 50, 50));
            }
        }

        g2d.dispose();
    }
}