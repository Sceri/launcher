package net.fuix.minecraft.launcher.wrapper;

/**
 * Created by Sceri 04.07.2016.
 */
public enum ResourceType {
    LIBRARIES("libraries"), MODS("mods"), PLUGINS("plugins"), NATIVES("libraries"), RESOURCES("resources");

    private final String folder;

    private ResourceType(String folder) {
        this.folder = folder;
    }

    public String getFolder() {
        return folder;
    }
}
