package net.fuix.minecraft.launcher.bootstrap;

import com.sun.management.OperatingSystemMXBean;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.net.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Создано Sceri 15.07.2015.
 */
public class Bootstrap extends JFrame implements MouseListener, MouseMotionListener {
    private static final File MAIN_DIRECTORY = getMainDirectory();
    private final BufferedImage SPLASH = getImage("splash");
    private final BufferedImage BAR_EMPTY = getImage("bar_1");
    private final BufferedImage BAR_FULL = getImage("bar_2");
    private static final int ALLOCATED_MEMORY = getAllocatedMemory();

//    private String status;
    private int percentage;

    private static String BASE_URL = "http://cobweb.fuix.net/";

    private Bootstrap() {
        super("CenturyMine Updater");

        JPanel panel = new JPanel(null) {
            @Override
            protected void paintComponent(Graphics g) {
                Graphics2D g2d = (Graphics2D) g;
                g2d.drawImage(SPLASH, 0, 0, SPLASH.getWidth(), SPLASH.getHeight(), null, null);
                g2d.drawImage(BAR_EMPTY, getWidth() / 2 - (BAR_EMPTY.getWidth() / 2), 85, null);

                if(percentage != 0) {
                    BufferedImage image = BAR_FULL.getSubimage(0, 0, (int) (((double) BAR_FULL.getWidth() / 100.0D) * (double) percentage), BAR_FULL.getHeight());
                    g2d.drawImage(image, getWidth() / 2 - (BAR_FULL.getWidth() / 2), 85, null);

//                    int amountFull = (int) (734 * (percentage / 100.0D));
//
//                    g2d.setColor(Color.black);
//                    g2d.fillRect(0, 263, 734, 12);
//
//                    g2d.setColor(new Color(63, 160, 221));
//                    g2d.fillRect(0, 263, amountFull, 12);
//
//                    paintString(g2d, 0, 263, 734, 12, 0, amountFull);
                }
            }

//            private void paintString(Graphics g, int x, int y, int width, int height, int fillStart, int amountFull) {
//                Graphics2D g2 = (Graphics2D) g;
//
//                g2.setFont(new Font("Arial", Font.BOLD, 12));
//                Point renderLocation = getStringPlacement(g2, status, x, y, width, height);
//                Rectangle oldClip = g2.getClipBounds();
//
//                g2.setColor(new Color(63, 160, 221));
//                SwingUtilities2.drawString(this, g2, status, renderLocation.x, renderLocation.y);
//                g2.setColor(Color.BLACK);
//                g2.clipRect(fillStart, y, amountFull, height);
//                SwingUtilities2.drawString(this, g2, status, renderLocation.x, renderLocation.y);
//                g2.setClip(oldClip);
//            }
//
//            private Point getStringPlacement(Graphics g, String progressString, int x, int y, int width, int height) {
//                FontMetrics fontSizer = SwingUtilities2.getFontMetrics(this, g, getFont());
//                int stringWidth = SwingUtilities2.stringWidth(this, fontSizer, progressString);
//
//                return new Point(x + Math.round(width / 2 - stringWidth / 2), y + ((height + fontSizer.getAscent() - fontSizer.getLeading() - fontSizer.getDescent()) / 2));
//            }
        };
        panel.setOpaque(false);

        addMouseListener(this);
        addMouseMotionListener(this);

        Timer timer = new Timer(40, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        });
        timer.start();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setIconImage(new ImageIcon(getImage("favicon")).getImage());
        setBackground(new Color(0, 0, 0, 0));
        setPreferredSize(new Dimension(SPLASH.getWidth(), SPLASH.getHeight()));

        getContentPane().add(panel, "Center");

        pack();
        setLocationRelativeTo(null);
        setVisible(true);

//        setStatus("Проверяем наличие обновлений", true);
        setFull();
        int dot = getCurrentLocation().getAbsolutePath().lastIndexOf(".");
        String extension = dot == -1 ? "jar" : getCurrentLocation().getAbsolutePath().substring(dot + 1);

        try {
            if(downloadFile(RepositoryType.SELECTEL, String.format("launcher/bootstrap.%s", extension),getCurrentLocation())) {
                relaunch();
            }
        } catch (BootstrapException e) {
            showError("Не удалось загрузить обновление.", e.getCause());
            destroy();
        }

        List<URL> urls = new ArrayList<>();

        try {
            setFull();
//            setStatus("Получаем данные с сервера", true);
            String launcherJson = performGetRequest(makeUrl(BASE_URL + "launcher"));
            if(launcherJson == null) {
                throw new IOException("Не удалось соединиться с сервером.");
            }

//            setStatus("Обновляем библиотеки лаунчера", true);

            JSONObject launcher = (JSONObject) JSONValue.parseWithException(launcherJson);
            JSONArray libraries = (JSONArray) launcher.get("libraries");
            for(Object object : libraries) {
                String library = (String) object;

                String[] parts = library.split(":");

                RepositoryType repo = RepositoryType.find(parts[0]);
                String groupId = parts[1];
                String artifactId = parts[2];
                String version = parts[3];
                String ext = parts.length == 4 ? "jar" : parts[4];

                String baseDir = String.format("%s/%s/%s", groupId.replaceAll("\\.", "/"), artifactId, version);
                String artifactFilename = String.format("%s-%s.%s", artifactId, version, ext);

                String path;
                File localFile;
                if(repo == RepositoryType.MOJANG) {
                    path = String.format("%s/%s", baseDir, artifactFilename);
                    localFile = new File(MAIN_DIRECTORY, File.separator + "libraries" + File.separator + path);
                } else {
                    path = String.format("%s/%s/%s", "libraries", baseDir, artifactFilename);
                    localFile = new File(MAIN_DIRECTORY, path);
                }

                downloadFile(repo, path, localFile);
                urls.add(localFile.toURI().toURL());
            }
        } catch (IOException e) {
            showError(e.getLocalizedMessage(), e);
            destroy();
        } catch (ParseException e) {
            showError("Не удалось обработать ответ от сервера.", e);
            destroy();
        } catch (BootstrapException e) {
            showError("Не удалось обновить библиотеки.", e.getCause());
            destroy();
        }

        setFull();
//        setStatus("Запускаем лаунчер", true);
        timer.stop();

        dispose();
        try {
            fixUrlConnection();

            URLClassLoader classLoader = new URLClassLoader(urls.toArray(new URL[urls.size()]), Thread.currentThread().getContextClassLoader());
            Thread.currentThread().setContextClassLoader(classLoader);

            Class<?> clazz = classLoader.loadClass("net.fuix.minecraft.launcher.Launcher");
            Constructor<?> constructor = clazz.getConstructor(JFrame.class);
            constructor.newInstance(this);
        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
            showError("Не удалось запустить лаунчер.", e);
            destroy();
        }
    }

    public static void main(String[] args) {
        boolean relaunched = false;
        for(String arg : args) {
            if(arg.equals("-relaunched")) {
                relaunched = true;
                break;
            }
        }

        if (!relaunched) {
            relaunch();
        }

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {
        }

        new Bootstrap();
    }

    private boolean downloadFile(RepositoryType repo , String path, File localFile) throws BootstrapException {
        this.percentage = 0;
//        setStatus(String.format("Загружаем %s", localFile.getName()), false);

        IOException lastException;

        URL remoteFile = makeUrl(String.format("%s/%s", repo.getUrl(), path));

        try {
            ensureFileWritable(localFile);
            HttpURLConnection connection = createUrlConnection(remoteFile, getMD5File(localFile));
            int responseCode = connection.getResponseCode();
            if (responseCode == 304) {
                return false;
            }
            if (responseCode / 100 == 2) {
                String md5 = copyAndDigest(new MonitoringInputStream(connection.getInputStream(), connection.getContentLength()), new FileOutputStream(localFile), "MD5", 32);
                String eTag = getETag(connection.getHeaderField("ETag"));

                if (md5.equalsIgnoreCase(eTag)) {
                    return true;
                }

                throw new IOException(String.format("Хешсумма скаченного файла (%s) не совпадает с удалённой (%s)", md5, eTag));
            }

            throw new IOException(String.format("Не удалось загрузить файл (%s). Ответ сервера: %s", remoteFile, responseCode));
        } catch (IOException ex) {
            lastException = ex;
        }

        throw new BootstrapException(lastException);
    }

    private void ensureFileWritable(File target) throws IOException {
        if (target.getParentFile() != null && !target.getParentFile().exists()) {
            target.getParentFile().mkdirs();
        }

        if (target.isFile() && !target.canWrite()) {
            throw new IOException(String.format("Нет разрешения на запись файла %s", target.getAbsolutePath()));
        }
    }

    private String copyAndDigest(InputStream inputStream, OutputStream outputStream, String algorithm, int hashLength) throws IOException {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);

            byte[] buffer = new byte[65536];
            try {
                int read = inputStream.read(buffer);
                while (read >= 1) {
                    messageDigest.update(buffer, 0, read);
                    outputStream.write(buffer, 0, read);
                    read = inputStream.read(buffer);
                }
            } finally {
                closeQuietly(inputStream);
                closeQuietly(outputStream);
            }

            return String.format("%1$0" + hashLength + "x", new BigInteger(1, messageDigest.digest()));
        } catch (NoSuchAlgorithmException ignored) {
        }

        return "";
    }

    private String performGetRequest(URL url) throws IOException {
        String response = null;

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(15000);
        connection.setUseCaches(false);
        connection.setDoInput(true);

        InputStream inputStream = null;
        try {
            inputStream = connection.getInputStream();

            StringBuilder stringBuilder = new StringBuilder();
            try(BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while((line = br.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }

                response = stringBuilder.toString();
            }
        } finally {
            closeQuietly(inputStream);
        }

        return response;
    }

    private HttpURLConnection createUrlConnection(URL url, String md5) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setUseCaches(false);
        connection.setDefaultUseCaches(false);
        connection.setRequestProperty("Cache-Control", "no-store,max-age=0,no-cache");
        connection.setRequestProperty("Expires", "0");
        connection.setRequestProperty("Pragma", "no-cache");

        if (md5 != null) {
            connection.setRequestProperty("If-None-Match", "\"" + md5 + "\"");
        }

        connection.setConnectTimeout(5000);
        connection.setReadTimeout(30000);

        return connection;
    }

    public static URL constantURL(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException ex) {
            throw new Error("Couldn't create constant for " + url, ex);
        }
    }

    private String getMD5File(File file) {
        try(InputStream fis = new FileInputStream(file)) {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            int numRead;
            while((numRead = fis.read(buffer)) != -1) {
                digest.update(buffer, 0, numRead);
            }

            return hashBytes(digest.digest());
        } catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static File getMainDirectory() {
        File userHome = new File(System.getProperty("user.home"));
        String osName = System.getProperty("os.name").toLowerCase();

        boolean isLinux = osName.contains("linux") || osName.contains("unix");
        boolean isWindows = osName.contains("win");
        boolean isMac = osName.contains("mac");

        File directory = isLinux ?
                new File(userHome, ".CenturyMine" + File.separator) : isWindows ?
                new File(new File(System.getenv("APPDATA")), ".CenturyMine" + File.separator) : isMac ?
                new File(userHome, "Library/Application Support/" + "CenturyMine" + File.separator) :
                new File(userHome, "CenturyMine" + File.separator);

        if(!directory.exists()) {
            directory.mkdir();
        }

        return directory;
    }

    private static int getAllocatedMemory() {
        String json = null;

        boolean autoMemory = true;
        try {
            File launcherJson = new File(MAIN_DIRECTORY, "launcher.json");

            if (launcherJson.exists()) {
                try (BufferedReader br = new BufferedReader(new FileReader(launcherJson))) {
                    StringBuilder sb = new StringBuilder();

                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }

                    json = sb.toString();
                }
            }

            if (json != null) {
                JSONObject object = (JSONObject) JSONValue.parseWithException(json);
                if(object != null ) {
                    JSONObject properties = (JSONObject) object.get("properties");
                    if (properties != null) {
                        Object auto = properties.get("autoMemory");
                        if(auto != null && auto instanceof Boolean) {
                            autoMemory = (Boolean) auto;
                        }

                        if(!autoMemory) {
                            Object memory = properties.get("memory");
                            if (memory != null && memory instanceof Number) {
                                return ((Number) memory).intValue();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        }

        if(autoMemory) {
            int[] memoryItems = new int[] {
                    256, 512, 1024, 1535, 2048, 3072, 4096, 8092, 16184
            };

            boolean is64 = System.getProperty("sun.arch.data.model").equals("64");
            int memoryLimit = (int) (((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / 1024 / 1024);
            return getClosestMemory(memoryItems, is64 ? memoryLimit / 2 : 1024);
        }

        return 1024;
    }

    private static void relaunch() {
        try {
            File file = new File(Bootstrap.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());

            ArrayList<String> params = new ArrayList<>();
            params.add(System.getProperty("java.home") + "/bin/java");
            params.add("-Xmx" + ALLOCATED_MEMORY + "m");
            params.add("-XX:+UseConcMarkSweepGC");
            params.add("-XX:+CMSIncrementalMode");
            params.add("-XX:-UseAdaptiveSizePolicy");
            params.add("-XX:MaxPermSize=128m");
            params.add("-XX:+DisableAttachMechanism");
            params.add("-classpath");
            params.add(file.getAbsolutePath());
            params.add(Bootstrap.class.getCanonicalName());
            params.add("-relaunched");

            ProcessBuilder pb = new ProcessBuilder(params);
            pb.directory(file.getParentFile());
            pb.start();
        } catch (Exception e) {
            showError(e.getLocalizedMessage(), e);
        }

        System.exit(0);
    }

    private static void showError(String error, Throwable exception) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);

        JOptionPane.showMessageDialog(null, String.format("Ошибка: %s\n\nStackTrace: %s\nСоздайте тему на форуме с подробным описанием ваших действий, если подобное повторится.", error, stringWriter.toString()), "Ошибка", JOptionPane.ERROR_MESSAGE);
    }

    private void fixUrlConnection() {
        try {
            Field field = URLConnection.class.getDeclaredField("defaultUseCaches");

            if (field != null) {
                field.setAccessible(true);
                field.set(null, true);
            }
        } catch (IllegalAccessException | NoSuchFieldException ignored) {
        }
    }

    private String hashBytes(byte[] bytes) {
        String result = "";
        for (byte aByte : bytes) {
            result += Integer.toString((aByte & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    private BufferedImage getImage(String name) {
        try {
            return ImageIO.read(Bootstrap.class.getResource("/" + name + ".png"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new BufferedImage(0, 0, 0);
    }

    private URL makeUrl(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException ignored) {
        }

        return null;
    }

    private static File getCurrentLocation() {
        try {
            return new File(Bootstrap.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
        } catch (URISyntaxException ignored) {
        }

        return new File(System.getenv("user.dir"));
    }

    private String getETag(String eTag) {
        if (eTag == null) {
            eTag = "-";
        } else if ((eTag.startsWith("\"")) && (eTag.endsWith("\""))) {
            eTag = eTag.substring(1, eTag.length() - 1);
        }
        return eTag;
    }

    public void setFull() {
        percentage = 100;
    }

//    public void setStatus(String status, boolean force) {
//        this.status = status;
//        if(force) {
//            percentage = 100;
//        }
//    }

    private void destroy() {
        setVisible(false);
        dispose();
        System.exit(0);
    }

    private void closeQuietly(Closeable closeable) {
        try {
            if(closeable != null) {
                closeable.close();
            }
        } catch (IOException ignored) {
            ;
        }

    }

    private static int getClosestMemory(int[] memoryItems, int memory) {
        int closest = memoryItems[0];
        int distance = Math.abs(closest - memory);
        for (int memoryItem : memoryItems) {
            int distanceI = Math.abs(memoryItem - memory);
            if (distance > distanceI) {
                closest = memoryItem;
                distance = distanceI;
            }
        }

        return closest;
    }

    public enum RepositoryType {
        MOJANG("https://libraries.minecraft.net"), SELECTEL("http://142408.selcdn.com/centurymine");

        final String url;

        RepositoryType(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        public static RepositoryType find(String name) {
            for(RepositoryType type : values()) {
                if(type.toString().equals(name)) {
                    return type;
                }
            }
            return SELECTEL;
        }
    }

    private class BootstrapException extends Exception {

        public BootstrapException(String message) {
            super(message);
        }

        public BootstrapException(String message, Throwable throwable) {
            super(message, throwable);
        }

        public BootstrapException(Throwable throwable) {
            super(throwable);
        }

    }

    private class MonitoringInputStream extends FilterInputStream {
        private final long totalSize;
        private long currentSize;

        protected MonitoringInputStream(InputStream in, long totalSize) {
            super(in);
            this.totalSize = totalSize;
        }

        @Override
        public int read() throws IOException {
            int result = in.read();
            if (result >= 0) {
                addProgress(1L);
            }

            return result;
        }

        @Override
        public int read(byte[] buffer) throws IOException {
            int size = in.read(buffer);
            if (size >= 0) {
                addProgress(size);
            }

            return size;
        }

        @Override
        public int read(byte[] buffer, int off, int len) throws IOException {
            int size = in.read(buffer, off, len);
            if (size > 0) {
                addProgress(size);
            }

            return size;
        }

        @Override
        public long skip(long size) throws IOException {
            long skipped = super.skip(size);

            if (skipped > 0L) {
                addProgress(skipped);
            }

            return skipped;
        }

        private void addProgress(long size) {
            this.currentSize += size;
            percentage = (int) ((double) this.currentSize / (double) this.totalSize * 100.0D);
        }
    }

    private int x, y;

    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {
        x = e.getX();
        y = e.getY();
    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseDragged(MouseEvent e) {
        setLocation(e.getX() + getX() - x, e.getY() + getY() - y);
    }

    public void mouseMoved(MouseEvent e) {

    }

}
